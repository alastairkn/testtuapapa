import "van11y-accessible-accordion-aria";

const Accordion = {
    init: () => {
        new van11yAccessibleAccordionAria();
    },
};

export default Accordion;
