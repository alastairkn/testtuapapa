import StoryCarousel from './elements/js/StoryCarousel';
import Media from './elements/js/Media';
import Accordion from './elements/js/Accordion';
import Tables from './elements/js/Tables';
import ImageCarousel from './elements/js/ImageCarousel';
import Tabs from './elements/js/Tabs';
import StaffTile from './elements/js/StaffTile';

document.addEventListener('DOMContentLoaded', () => {
    StoryCarousel.initCarousel();
    StoryCarousel.mobileCarousel();
    Media.bindEvents();
    Accordion.init();
    Tables.init();
    StaffTile.init();
    ImageCarousel.initCarousel();
    document.querySelector('.tablinks') ? Tabs.init() : null;
});
