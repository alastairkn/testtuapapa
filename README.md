# Tuapapa Package

Tuapapa package installs elemental blocks and page types required for all the tuapapa sites.


## Installation

Installation via composer

```bash
$ composer require tuapapa/tuapapa-package
```
