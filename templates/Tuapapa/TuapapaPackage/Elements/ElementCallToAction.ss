<div class="$ClassName.Shortname.Lowercase">
    <% if $Image %>
        <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\CallToActionImageBlock %>
    <% else %>
        <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\CallToActionBlock %>
    <% end_if %>
</div>
