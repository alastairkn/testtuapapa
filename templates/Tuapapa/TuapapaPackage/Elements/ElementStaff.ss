<div class="mb-12 md:mb-24 px-4 md:px-6 lg:px-4 xl:px-0 md:max-w-7xl mx-auto $ClassName.Shortname.Lowercase">
    <% if $Title && $ShowTitle %>
        <h2 class="m-8 font-semibold text-center text-primaryColour">$Title</h2>
    <% end_if %>
    <% if $Intro %>
        <div class="mx-auto mb-10 text-center md:max-w-4xl font-headingRegular mb-12">{$Intro}</div>
    <% end_if %>
    <% if $AllStaff %>
        <div class="grid gap-x-5 gap-y-12 grid-cols-1 md:grid-cols-2 lg:grid-cols-4">
            <% cached $StaffItemsCacheKey %>
                <% loop $AllStaff %>
                    <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\StaffTile ManualStaffSelection=$Up.ManualStaffSelection, Category=$Up.Category.Title %>
                <% end_loop %>
            <% end_cached %>
        </div>
    <% end_if %>
</div>
