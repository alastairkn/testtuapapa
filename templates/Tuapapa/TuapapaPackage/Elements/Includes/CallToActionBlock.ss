<div class="<% if $BackgroundColour %>{$BackgroundColour.ColourClassName}<% end_if %>">
    <div class="mx-auto md:p:0 c-container">
        <div class="px-4 md:px-4 py-14 lg:py-24">
            <div class="grid grid-cols-6 md:grid-cols-8 lg:grid-cols-12">
                <div class="col-span-6 md:col-start-2 md:col-span-6 lg:col-start-3 lg:col-span-8">
                    <div class="px-4 md:px-0">
                        <% if $Title && $ShowTitle %>
                            <h2 class="text-center <% if not $BackgroundColour.TextColour %>text-primaryColour<% end_if %>">
                                {$Title}
                            </h2>
                            <div class="w-24 mx-auto mt-5 h-1.5 <% if $BackgroundColour.TextColour %>{$BackgroundColour.ColourClassName}-button<% else %> bg-primaryColour<% end_if %>"></div>
                        <% end_if %>
                        <% if $Content %>
                            <p class="pt-8 text-center <% if not $BackgroundColour.TextColour %>text-black<% end_if %>">$Content</p>
                        <% end_if %>
                        <% if $LinkItem.ID %>
                            <div class='flex justify-center mt-7'>
                                <a aria-label="$LinkItem.Title" href="<% if $LinkItem.Phone %>tel:{$LinkItem.Phone}<% else %>{$LinkItem.LinkURL}<% end_if %>" class="btn <% if $BackgroundColour.TextColour %>{$BackgroundColour.ColourClassName}-button<% else %> btn-primary<% end_if %>" $LinkItem.TargetAttr>
                                    $LinkItem.Title
                                </a>
                            </div>
                        <% end_if %>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
