<% cached $Top.ImagesCacheKey %>
    <div class="flex flex-wrap justify-center mx-auto max-w-px-1000">
        <% loop $Images %>
            <div class="
                <% if $Pos == 1 %>md:w-1/2 pr-20 md:pr-14 lg:pr-28 mb-8 md:mb-0<% end_if %>
                <% if $Pos == 2 %>md:w-1/2 pl-24 md:pl-14 lg:pl-28 mb-8 flex flex-col-reverse align-center justify-end self-end<% end_if %>
                <% if $Pos == 3 %>md:w-2/3 lg:w-1/2 pr-12  md:pr-20 lg:pr-20 pt-8 md:pt-14 lg:pt-28<% end_if %>
            ">
                <div class="w-full media-container <% if $BackgroundColour.ColourClassName %>media-offset<% end_if %>">
                    <div class="
                        <% if $BackgroundColour.ColourClassName %>$BackgroundColour.ColourClassName bg-colour-offset
                            <% if $Pos == 2 %>
                                self-end
                            <% end_if %>
                        <% end_if %>">
                        <img src="{$Image.URL}" loading="lazy" alt="{$Image.Title}" class="stacked-<% if $Pos == 1 %>1<% end_if %><% if $Pos == 2 %>2<% end_if %><% if $Pos == 3 %>3<% end_if %>">
                    </div>
                </div>
                <p class="image-caption <% if $BackgroundColour.ColourClassName && $Pos != 2 %>text-offset<% end_if %> <% if $Pos == 2 %>mb-4 md:mb-5<% end_if %> ">{$Caption}</p>
            </div>
        <% end_loop %>
    </div>
<% end_cached %>
