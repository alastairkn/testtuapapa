<% if $Image %>
    <%-- Single Image layout --%>
    <div class="
        <% if $ImagePosition %>
            story_image-right
        <% else %>
            story_image-left
        <% end_if %>
        story_image flex
        <% if $ImageOffset %> story_image-bottom <% else %>story_image-top <% end_if %>
        <% if $ImagePosition %>justify-end <% else %>justify-start <% end_if %>
        md:w-1/2">
        <picture class="flex w-full h-full">
            <!--[if IE 9]><video style="display: none"><![endif]-->
            <% loop $ImageSources %>
                <source
                data-srcset="{$Up.Image.URL}?{$Params}{$Up.ImageCommonParams}&crop={$Up.ImageCrop}&q=100 1x,
                {$Up.Image.URL}?{$Params}{$Up.ImageCommonParams}&crop={$Up.ImageCrop}&q=50&dpr=2 2x"
                <% if $MaxWidth %>
                    media="(max-width: {$MaxWidth})"
                <% end_if %>
                />
            <% end_loop %>
            <!--[if IE 9]></video><![endif]-->
            <img
            src="{$Image.URL}?{$ImagePlaceholderParams}{$ImageCommonParams}&crop={$ImageCrop}&q=100"
            data-src="{$Image.URL}?{$ImageDefaultParams}{$ImageCommonParams}&crop={$ImageCrop}&q=100"
            class="lazyload"
            alt="$Title.ATT"/>
        </picture>
    </div>
<% end_if %>
