<div class="grid grid-cols-4 md:grid-cols-8 lg:grid-cols-12">
    <div class="col-span-4 md:col-span-4 lg:col-span-8 <% if $CarouselPosition %>col-start-1 md:col-start-5 lg:col-start-5<% end_if %>">
        <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ImageCarousel %>
    </div>
    <div class="flex flex-col justify-center col-span-4 py-6 md:py-10 lg:py-12 md:col-span-4 lg:col-span-3 md:col-span-3 <% if $CarouselPosition %>mr-0 md:mr-10 lg:mr-10 col-start-1 md:col-start-1 lg:col-start-1 row-start-1<% else %>ml-0 md:ml-10 lg:ml-0 lg:col-start-10<% end_if %>">
        <% if $Title && $ShowTitle %>
            <h3 class="text-primaryColour mb-3 md:mb-5 lg:mb-9">$Title</h3>
        <% end_if %>
        <p class="text-black">$Content</p>
        <% if $LinkItem %>
            <div class="mt-3 md:mt-5 lg:mt-20">
                <a aria-label="$LinkItem.Title" href="$LinkItem.URL" $LinkItem.TargetAttr class="link link--content font-bodyHeavy"><span class="text-primaryColour">$LinkItem.Title</span></a>
            </div>
        <% end_if %>
    </div>
</div>
