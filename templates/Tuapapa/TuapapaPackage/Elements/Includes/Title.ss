<% if $IsEventPage %>
    <% if $EventCategories %>
        <h1 class="uppercase event-heading tracking-widest small-size mb-1">
            <span class="<% if $TextBackgroundColour %>{$TextBackgroundColour.ColourClassName}<% else %>text-primaryColour<% end_if %>"><% loop $EventCategories %><% if $Last %>$Title<% else %>$Title, <% end_if %><% end_loop %></span>
        </h1>
    <% end_if %>
    <h2 class="event-title mb-8 event-heading">
        <span class="<% if $TextBackgroundColour %>{$TextBackgroundColour.ColourClassName}<% end_if %> <% if not $TextBackgroundColour && not $BackgroundColourID && not $BackgroundSplit %>text-primaryColour<% end_if %>">{$Title.RAW}</span>
    </h2>
    <% if $Content %>
        <div class="grid grid-cols-4 px-4 pb-12 c-container md:px-4 lg:px-0 md:grid-cols-8 lg:grid-cols-12">
            <div class="col-span-4 md:col-start-2 md:col-span-6 lg:col-span-8 lg:col-start-3 event-intro">
                {$Content}
            </div>
        </div>
    <% end_if %>
<% else %>
    <% if $Title && $ShowTitle || $SubTitle %>
        <div class="lg:px-4 xl:px-0 md:max-w-px-800 lg:max-w-px-1200 mx-auto z-10
            <% if $BackgroundType == 'Image' || $BackgroundType == 'Video' %>
                <% if not $TextBackgroundColour  %>
                    text-primaryColour
                <% end_if %>
                mb-0
            <% else %>
                mb-8
                <% if not $TextBackgroundColour && not $BackgroundColourID && not $BackgroundSplit %>text-primaryColour<% end_if %>
            <% end_if %>
            ">
            <% if $Title && $ShowTitle  %>
                <h1 class="
                    <% if $SubTitle %>
                        mb-1
                    <% else %>
                        mb-0
                    <% end_if %>
                    <% if not $TextBackgroundColour && not $BackgroundColourID && not $BackgroundSplit && $BackgroundType %>text-primaryColour<% end_if %>
                    <% if not $isShortCoursePage %><% if $TitleSize == 'Large' %> large-size <% else_if $TitleSize == 'Medium' %> medium-size <% else %> small-size <% end_if %><% end_if %>
                    <% if $isShortCoursePage %> small-size<% end_if %>
                    <% if $TitleFont == 'Secondary' %> secondary-font <% else %> font-bodyHeavy <% end_if %>
                    <% if $TitleStyle == 'LevelTwo' %> level-two
                    <% else_if $TitleStyle == 'Uppercase' %> uppercase <% end_if %>
                    ">
                    <span class="
                        <% if $TextBackgroundColour %>
                            {$TextBackgroundColour.ColourClassName} py-1.5 md:py-2.5 px-2.5 md:px-5 colour-bg-text-leading
                        <% end_if %>
                        <% if not $TextBackgroundColour || $TextBackgroundColour.ColourClassName == 'passion-text' %> tracking-normal <% end_if %>">
                        <% if $isShortCoursePage %>{$SubTitle.RAW}<% else %>{$Title.RAW}<% end_if %>
                    </span>
                </h1>
            <% end_if %>
            <% if $SubTitle %>
                <h2 class="
                    subtitle
                    <% if not $TextBackgroundColour && not $BackgroundColourID && not $BackgroundSplit %>text-primaryColour<% end_if %>
                    <% if $BackgroundType == 'None'  %>
                        mb-0
                    <% end_if %>
                    secondary">
                    <span class="<% if $TextBackgroundColour %>{$TextBackgroundColour.ColourClassName} py-1.5 md:py-2.5 px-2.5 md:px-5 colour-bg-text-leading<% end_if %>"><% if $isShortCoursePage %>{$Title.RAW}<% else %>{$SubTitle.RAW}<% end_if %></span>
                </h2>
            <% end_if %>
        </div>
    <% end_if %>
<% end_if %>
