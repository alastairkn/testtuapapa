<%-- Tile data --%>
<div class="staff-card">
    <div class="staff-card__info">
        <% if $Image %>
            <div class="mb-4">
            <% if $DisplayModal %> <button class="no-link-style" data-open="staff-modal-$ID"><% end_if %>
                <picture class="staff-card__photo flex w-full h-full">
                    <!--[if IE 9]><video style="display: none"><![endif]-->
                    <% loop $ImageSources %>
                        <source
                            data-srcset="{$Up.Image.URL}?{$Params}{$Up.ImageCommonParams}&crop={$Up.ImageCrop}&q=100 1x,
                            {$Up.Image.URL}?{$Params}{$Up.ImageCommonParams}&crop={$Up.ImageCrop}&q=50&dpr=2 2x"
                            <% if $MaxWidth %>
                                media="(max-width: {$MaxWidth})"
                            <% end_if %>
                        />
                    <% end_loop %>
                    <!--[if IE 9]></video><![endif]-->
                    <img
                        src="{$Image.URL}?{$ImagePlaceholderParams}{$ImageCommonParams}&crop={$ImageCrop}&q=100"
                        data-src="{$Image.URL}?{$ImageDefaultParams}{$ImageCommonParams}&crop={$ImageCrop}&q=100"
                        class="lazyload w-full h-full object-cover"
                        alt="$Name"
                    />
                </picture>
            <% if $DisplayModal %></button><% end_if %>
            </div>
        <% end_if %>
        <% if $Category %>
            <div class="staff-card__category leading-none mb-4 lg:mb-5">
                $Category
            </div>
        <% end_if %>
        <h3 class="staff-card__name text-primaryColour">
            $Name
        </h3>
        <div class="staff-card__jobtitle mb-4 md:mb-5">
            <% if $Categories.Filter('Title', $Category ) %>
                <% loop $Categories.Filter('Title', $Category ) %>
                    <% if $JobTitle %>
                        $JobTitle
                    <% else %>
                        $Top.JobTitle
                    <% end_if %>
                <% end_loop %>
            <% else %>
                $Top.JobTitle
            <% end_if %>
        </div>
    </div>
    <%-- If the readmore link field is set in the CMS then we want to link to that otherwise we should open the modal --%>
    <% if $DisplayModal %>
        <a href="/" class="link link--default font-bodyHeavy link--content"
        data-open="staff-modal-$ID">Read more</a>
    <% end_if %>
</div>

<%-- Modal data below / We should only render if there's not a ReadMore link set --%>
<% if $DisplayModal %>
    <div id="staff-modal-$ID" class="modal-container inset-0 z-50 backdrop-blur-sm items-center content-center pt-10 w-full h-full pr-4 pl-6 md:px-6 fixed" tabindex="-1" aria-hidden="true">
        <!-- $Name Modal container -->
        <div class="modal-content relative mx-auto">
            <%-- Close Button --%>
            <button
                aria-label="close button"
                class="block text-5xl right-0 top-0 text-blue-900 absolute z-30 cursor-pointer"
                data-close
            >
                <svg class="no-underline" role="presentation" width="60" height="60" xmlns="http://www.w3.org/2000/svg"><path d="m37.099 21.5 1.4 1.4-7.143 7.142 2.15 2.15 4.908 4.907-1.4 1.4-5.657-5.657-1.4-1.4-7.014 7.014-1.4-1.4 7.014-7.014-7.057-7.057 1.4-1.4 7.056 7.057 7.143-7.142z" class="fill-current <% if $Fill %>$Fill<% else %>text-primaryColour<% end_if %>" fill-rule="evenodd"/></svg>
            </button>
            <div class="modal-body max-h-lg md:max-h-2xl max-w-xl bg-white relative z-20 px-6 md:px-9 pb-16 pt-24 overflow-y-scroll">
                <div class="mb-6">
                    <h3 class="staff-card__name text-primaryColour mb-1 text-center">$Name</h3>
                    <% if $Categories %>
                        <h4 class="staff-card__jobtitle text-center">
                            <% loop $Categories.Filter('Title', $Category ) %>
                                <% if $JobTitle %>
                                    $JobTitle
                                <% else %>
                                    $Top.JobTitle
                                <% end_if %>
                            <% end_loop %>
                        </h4>
                    <% end_if %>
                </div>
                <div class="mb-8 text-center">
                    $Bio
                </div>
                <% if $Link1 || Link2 %>
                    <div class="modal__links md:flex md:justify-between">
                        <% if $Link1 %>
                            <div class="<% if $Link2 %>mb-5 md:mb-0<% end_if %>">
                                <a class="link link--default font-bodyHeavy link--content text-base" href="{$Link1.LinkURL}" {$Link1.TargetAttr}>$Link1.Title</a>
                            </div>
                        <% end_if %>
                        <% if $Link2 %>
                            <div>
                                <a class="link link--default font-bodyHeavy link--content text-base" href="{$Link2.LinkURL}" {$Link2.TargetAttr}>$Link2.Title</a>
                            </div>
                        <% end_if %>
                    </div>
                <% end_if %>
                <% if $RelatedLinks %>
                    <h4 class="tracking-normal font-bodyHeavy text-primaryColour mt-11 mb-4 text-left text-xl">Related links</h4>
                    <div class="bg-sky p-8">
                        <ul>
                            <% loop $RelatedLinks.Sort(RelatedLinksSortOrder, ASC) %>
                                <li class="mb-4 last:mb-0 text-primaryColour">
                                    $Link
                                </li>
                            <% end_loop %>
                        </ul>
                    </div>
                <% end_if %>
            </div>
        </div>
    </div>
<% end_if %>
