<% if $DisplayTab('Domestic') || $DisplayTab('International') %>
    <div class="mb-8 md:mb-12 element-tabs max-w-7xl mx-auto">
        <div class="md:max-w-4xl md:mx-auto text-primaryColour print-hide">
            <div class="flex">
                <% if $DisplayTab('Domestic') %>
                  <div class="cursor-pointer tablinks active" data-tab="Domestic">Domestic</div>
                <% end_if %>
                <% if $DisplayTab('International') %>
                  <div class="cursor-pointer tablinks" data-tab="International">International</div>
                <% end_if %>
            </div>
        </div>
        <div class="w-full border-b border-grey-300 print-hide"></div>
        <div class="md:max-w-4xl md:mx-auto text-primaryColour">
            <% if $DisplayTab('Domestic') %>
                <div id="Domestic" class="tabcontent">
                    <div class="hidden print-show tablinks"><h4 class="w-full border-b border-grey-300">Domestic</h4></div>
                    <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeBoxes TabBoxes=$DomesticBoxes %>
                    <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeApplyLinks %>
                    <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeTabContent TabContent=$TabContentDomestic, TextColourClass=text-black %>
                </div>
            <% end_if %>
             <% if $DisplayTab('International') %>
                <div id="International" class="hidden tabcontent">
                    <div class="hidden mt-12 print-show tablinks"><h4 class="w-full border-b border-grey-300">International</h4></div>
                    <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeBoxes TabBoxes=$InternationalBoxes %>
                    <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeApplyLinks %>
                    <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeTabContent TabContent=$TabContentInternational, TextColourClass=text-black %>
                </div>
            <% end_if %>
        </div>
    </div>
<% end_if %>
