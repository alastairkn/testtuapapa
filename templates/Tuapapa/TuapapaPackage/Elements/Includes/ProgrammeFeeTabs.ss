<% if TabContentWhatYouStudy || $TabContentWorkload || $TabContentEntry || $TabContentFees || $TabContentFees %>
    <div class="mx-auto mb-8 md:mb-8 max-w-7xl element-tabs">
        <div class="flex md:max-w-4xl md:mx-auto text-primaryColour scroll-container print-hide">
            <div id="left-scroll" class="flex items-center h-12 pr-3 md:hidden text-primaryColour">
                <% include App\\Includes\\Icons\\LeftArrowLong %>
            </div>
            <div class="flex -mb-4 overflow-x-scroll md:overflow-hidden whitespace-nowrap md:m-0" id="scrolling-tabs">
                <% if $TabContentWhatYouStudy %>
                <div class="cursor-pointer tablinks2 active" data-tab="WhatYouStudy">What you study</div>
                <% end_if %>
                <% if $TabContentWorkload %>
                <div class="cursor-pointer tablinks2" data-tab="Workload">Workload</div>
                <% end_if %>
                <% if $TabContentEntry %>
                <div class="cursor-pointer tablinks2" data-tab="Entry">Entry</div>
                <% end_if %>
                <% if $TabContentFees %>
                <div class="cursor-pointer tablinks2" data-tab="Fees">Fees</div>
                <% end_if %>
                <% if $TabContentApplication %>
                <div class="cursor-pointer tablinks2" data-tab="Application">Application</div>
                <% end_if %>
            </div>
            <div id="right-scroll" class="flex items-center h-12 pl-3 md:hidden">
                <% include App\\Includes\\Icons\\RightArrowLong %>
            </div>
        </div>
        <div class="w-full border-b border-grey-300 print-hide"></div>
        <div class="md:max-w-4xl md:mx-auto text-primaryColour">

            <% if $TabContentWhatYouStudy %>
                <div id="WhatYouStudy" class="tabcontent2">
                    <div class="hidden mt-12 print-show"><h4 class="w-full border-b border-grey-300">What You Study</h4></div>
                    <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeTabContent TabContent=$TabContentWhatYouStudy, TextColourClass=text-black %>
                </div>
            <% end_if %>

            <% if $TabContentWorkload %>
                <div id="Workload" class="hidden tabcontent2">
                    <div class="hidden mt-12 print-show"><h4 class="w-full border-b border-grey-300">Workload</h4></div>
                    <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeTabContent TabContent=$TabContentWorkload, TextColourClass=text-black %>
                </div>
            <% end_if %>

            <% if $TabContentEntry %>
                <div id="Entry" class="hidden tabcontent2">
                    <div class="hidden mt-12 print-show"><h4 class="w-full border-b border-grey-300">Entry</h4></div>
                    <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeTabContent TabContent=$TabContentEntry, TextColourClass=text-black %>
                </div>
            <% end_if %>

            <% if $TabContentFees %>
                <div id="Fees" class="hidden tabcontent2">
                    <div class="hidden mt-12 print-show"><h4 class="w-full border-b border-grey-300">Fees</h4></div>
                    <div class="grid grid-cols-1 md:grid-cols-2 print:grid-cols-2">
                        <% if $DomesticFeeBoxes %>
                            <div class="mr-4">
                                <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeFeeBoxes Title="Domestic fees", TabBoxes=$DomesticFeeBoxes %>
                            </div>
                        <% end_if %>
                        <% if $DomesticFeeBoxes %>
                            <div class="mr-4">
                                <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeFeeBoxes Title="International fees", TabBoxes=$InternationalFeeBoxes %>
                            </div>
                        <% end_if %>
                    </div>

                    <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeTabContent TabContent=$TabContentFees, TextColourClass=text-black %>

                    <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeTabHalfContent TabContent=$ContentLibraryItems, TextColourClass=text-black %>
                </div>
            <% end_if %>

            <% if $TabContentApplication %>
                <div id="Application" class="hidden tabcontent2">
                    <div class="hidden mt-12 print-show"><h4 class="w-full border-b border-grey-300">Application</h4></div>
                    <% include Tuapapa\\TuapapaPackage\\Elements\\Includes\\ProgrammeTabContent TabContent=$TabContentApplication, TextColourClass=text-black %>
                </div>
            <% end_if %>

        </div>
    </div>
<% end_if %>
