<div class="break-words md:flex <% if $ImageOffset %>mb-10 md:mb-0 mt-10 md:mt-0 lg:mt-14 <% else %>md:items-center<% end_if %> px-4 md:pl-10 md:pr-2 lg:px-8 xl:px-16
    <% if $ImagePosition %>
        xl:pl-32
        lg:pr-20
        mt-10
    <% else %>
        lg:pr-4 xl:pr-32
    <% end_if %>
    <% if not $ImagePosition && not $ImageOffset %>
        md:mt-10
    <% end_if %>
     lg:w-1/2
     md:w-1/2">
    <div class="<% if $BackgroundColour && $BackgroundColour.ColourClassName != 'op-grey-background' %>text-white<% else %>text-blue-500<% end_if %>">
        <% if $Category %>
            <p class="story-category">$Category</p>
        <% end_if  %>
        <% if $Title && $ShowTitle %>
            <h2 class="story-title <% if not $BackgroundColour.TextColour %>text-primaryColour<% end_if %>">
                {$Title}
            </h2>
        <% end_if %>
        <% if $SubTitle %>
            <p class="story-content-text mb-5">$SubTitle</p>
        <% end_if %>
        <% if $Content %>
            <p class="story-content-text">$Content.LimitCharacters(350,'...')</p>
        <% end_if %>
        <% if $LinkItem %>
            <div class="mt-5 lg:mt-10">
                <a aria-label="$LinkItem.Title" href="<% if $LinkItem.Phone %>tel:{$LinkItem.Phone}<% else %>{$LinkItem.LinkURL}<% end_if %>" $LinkItem.TargetAttr  class="btn inline-block <% if $BackgroundColour && $BackgroundColour.ColourClassName != 'op-grey-background' %>btn-secondary<% else %>btn-primary<% end_if %>">$LinkItem.Title</a>
            </div>
        <% end_if %>
    </div>
</div>
