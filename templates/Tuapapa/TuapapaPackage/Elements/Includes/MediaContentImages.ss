<% if $Images %>
    <% loop $Images %>
        <div class="alternating-images flex flex-wrap mb-20 md:mb-32 items-center mx-auto max-w-px-1000">
            <div class="w-full md:w-2/5 media-container">
                <div class="alternating__image mb-7 md:mb-0 <% if $BackgroundColour.ColourClassName %>$BackgroundColour.ColourClassName bg-colour-offset <% end_if %>">
                    <img src="{$Image.Fill(400, 480).URL}" loading="lazy" alt="{$Image.Title}" class="step-image" />
                </div>
            </div>
            <div class="w-full md:w-3/5">
                <div class="m-auto alternating__content">
                    <h3 class="text-primaryColour font-bodyHeavy mb-4 md:mb-10 md:mt-0">{$Title}</h3>
                    <div class="text-black font-body">$Content.LimitCharacters(350,' ...')</div>
                    <% if $LinkItem.ID %>
                        <div class="mt-7">
                            <a aria-label="$LinkItem.Title" href="<% if $LinkItem.Phone %>tel:{$LinkItem.Phone}<% else %>{$LinkItem.LinkURL}<% end_if %>" class="link link--default font-bodyHeavy link--content" $LinkItem.TargetAttr>
                                $LinkItem.Title
                            </a>
                        </div>
                    <% end_if %>
                </div>
            </div>
        </div>
    <% end_loop %>
<% end_if %>
