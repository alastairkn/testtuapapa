<% if $ContentLibraryItems %>
    <div class="grid grid-cols-1 md:grid-cols-2 body-content">
       <% loop $ContentLibraryItems %>
           <div class="pr-8 mt-12">
                <h2 class="text-4xl no-margin-top library-heading">{$Title}</h2>
                <div class="<% if $Top.TextColourClass %>{$Top.TextColourClass}<% end_if %>">{$Content}</div>
                <% if $LinkItem %>
                    <a aria-label="$LinkItem.Title" href="<% if $LinkItem.Phone %>tel:{$LinkItem.Phone}<% else %>{$LinkItem.LinkURL}<% end_if %>" class="link--content" $LinkItem.TargetAttr>$LinkItem.Title</a>
                <% end_if %>
           </div>
        <% end_loop %>
    </div>
<% end_if %>
