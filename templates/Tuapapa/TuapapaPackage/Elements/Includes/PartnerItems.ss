<a aria-label="$Title" href="$Link.URL" $Link.TargetAttr class="w-full h-full grid-link no-link-style md:w-1/2 lg:w-1/3">
    <div class="flex flex-col p-6 text-center bg-white border-solid border-1 md:p-8 partner-item text-primaryColour hover:bg-primaryColour hover:text-white">
        <% if $DefaultLogo || $HoverLogo %>
            <div class="logo-holder mb-4 h-32 w-auto flex items-center justify-center">
                <% if $DefaultLogo %>
                    <picture class="">
                        <!--[if IE 9]><video style="display: none"><![endif]-->
                        <% loop $ImageSources %>
                            <source
                                data-srcset="{$Up.DefaultLogo.URL}?{$Params}{$Up.ImageCommonParams}&crop={$Up.ImageCrop}&q=100 1x,
                                {$Up.DefaultLogo.URL}?{$Params}{$Up.ImageCommonParams}&crop={$Up.ImageCrop}&q=50&dpr=2 2x"
                                <% if $MaxWidth %>
                                    media="(max-width: {$MaxWidth})"
                                <% end_if %>
                            />
                        <% end_loop %>
                        <!--[if IE 9]></video><![endif]-->
                        <img
                            src="{$DefaultLogo.URL}?{$ImagePlaceholderParams}{$ImageCommonParams}&crop={$ImageCrop}&q=100"
                            data-src="{$DefaultLogo.URL}?{$ImageDefaultParams}{$ImageCommonParams}&crop={$ImageCrop}&q=100"
                            class="lazyload default-img w-auto h-full max-h-32 opacity-100"
                            alt="$Title.ATT"
                        />
                    </picture>
                <% end_if %>
                <% if $HoverLogo %>
                    <picture>
                        <!--[if IE 9]><video style="display: none"><![endif]-->
                        <% loop $ImageSources %>
                            <source
                                data-srcset="{$Up.HoverLogo.URL}?{$Params}{$Up.ImageCommonParams}&crop={$Up.ImageCrop}&q=100 1x,
                                {$Up.HoverLogo.URL}?{$Params}{$Up.ImageCommonParams}&crop={$Up.ImageCrop}&q=50&dpr=2 2x"
                                <% if $MaxWidth %>
                                    media="(max-width: {$MaxWidth})"
                                <% end_if %>
                            />
                        <% end_loop %>
                        <!--[if IE 9]></video><![endif]-->
                        <img
                            src="{$HoverLogo.URL}?{$ImagePlaceholderParams}{$ImageCommonParams}&crop={$ImageCrop}&q=100"
                            data-src="{$HoverLogo.URL}?{$ImageDefaultParams}{$ImageCommonParams}&crop={$ImageCrop}&q=100"
                            class="lazyload hover-img opacity-0 w-0 max-h-32"
                            alt="$Title.ATT"
                        />
                    </picture>
                <% end_if %>
            </div>
        <% else %>
            <div class="mb-4 px-2 w-full h-32 flex items-center justify-center">
                <h3 class='font-headingBold whitespace-normal text-center mb-0'>$Title.LimitCharacters(40,'...')</h3>
           </div>
        <% end_if %>
        <p class="text-center whitespace-normal font-body md:max-h-40 mb-0">
            {$Content.LimitCharacters(128,'...')}
        </p>
    </div>
</a>

