<div class="$ClassName.Shortname.lowercase c-container">
    <div class="px-4 my-6 md:my-8 md:px-4 lg:px-0 md:my-10 lg:my-12">
        <div class="grid grid-cols-4 md:grid-cols-8 lg:grid-cols-12">
            <div class="col-span-4 md:col-start-2 md:col-span-6 lg:col-span-8 lg:col-start-3">
                <div class="w-20 h-1.5 mb-4 bg-secondaryColour"></div>
                <% if $Quote %>
                    <p class="mt-5 mb-8 md:mb-10 text-2xl font-semibold leading-10 md:text-3xl text-primaryColour font-headingBold">&ldquo;{$Quote}&rdquo;</p>
                <% end_if %>
                <% if $QuoteAuthor %>
                    <p class="my-1 tracking-widest text-base uppercase text-primaryColour">{$QuoteAuthor}</p>
                <% end_if %>
                <% if $Role %>
                    <p class="my-1 font-bodyMedium">$Role</p>
                <% end_if %>
                <% if $Industry %>
                    <p>$Industry</p>
                <% end_if %>
            </div>
        </div>
    </div>
</div>
