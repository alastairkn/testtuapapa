<div class="$ClassName.Shortname.Lowercase <% if not $ShowSuccessMessage %>my-8 md:py-12 lg:py-16 <% end_if %>" id="enquiry-form">
    <div class="c-container">
        <div class="px-4 md:px-4 lg:px-0">
            <div class="grid grid-cols-4 md:grid-cols-8 lg:grid-cols-12">
                <div class="col-span-4 md:col-start-2 md:col-span-6 lg:col-span-8 lg:col-start-3">
                    <% if not $ShowSuccessMessage %>
                        <% if $Title && $ShowTitle %>
                            <h2 class="col-span-12 tracking-normal text-center text-primaryColour font-bodyHeavy md:col-span-8 md:col-start-3 lg:-col-start-4">{$Title}</h2>
                        <% end_if %>
                    <% end_if %>
                    <div class="relative col-span-12 mt-8 md:mt-12 md:col-span-8 md:col-start-3 lg:-col-start-4">
                        $ContactForm
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
