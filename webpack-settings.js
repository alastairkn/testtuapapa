module.exports = {
    externalJS: require("./loaders/js/externals"),
    moduleJS: require("./loaders/js/modules"),
    pluginJS: require("./loaders/js/plugins"),
    resolveJS: require("./loaders/js/resolve"),
    moduleCSS: require("./loaders/css/modules"),
    pluginCSS: require("./loaders/css/plugins"),
};
