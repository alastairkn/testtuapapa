<?php

namespace Tuapapa\TuapapaPackage\Pages;

use Tuapapa\TuapapaPackage\Elements\ElementMedia;
use Tuapapa\TuapapaPackage\Pages\ProgrammeInfoPage;
use Tuapapa\TuapapaPackage\Elements\ElementCarousel;
use Tuapapa\TuapapaPackage\Models\QualificationType;
use Tuapapa\TuapapaPackage\Elements\ElementAccordion;
use DNADesign\Elemental\Extensions\ElementalAreasExtension;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Forms\LiteralField;
use Tuapapa\TuapapaPackage\Elements\ElementDetailsBlock;
use SilverStripe\Forms\DropdownField;
use Tuapapa\TuapapaPackage\Elements\ElementGenericContent;
use Tuapapa\TuapapaPackage\Elements\ElementHeader;
use Tuapapa\TuapapaPackage\Models\Location;

/**
 * Class ShortCoursePage
 *
 * @package Tuapapa\TuapapaPackage\Pages
 */
class ShortCoursePage extends ProgrammeInfoPage
{
    /**
     * @var string
     */
    private static $table_name = 'App_ShortCoursePage';

    /**
     * @var string
     */
    private static $singular_name = 'Short Course';

    /**
     * @var string
     */
    private static $plural_name = 'Short Courses';

    /**
     * @var string
     */
    private static $default_sort = '"Title" ASC';

    /**
     * @var string[]
     */
    private static $has_one = [
        'Location' => Location::class
    ];

    /**
     * Enable thirdparty elements
     *
     * @config
     * @var string[]
     */
    private static $allowed_elements = [
        ElementHeader::class,
        ElementAccordion::class,
        ElementGenericContent::class,
        ElementCarousel::class,
        ElementDetailsBlock::class
    ];

    /**
     * Disable thirdparty elements
     *
     * @config
     * @var string[]
     */
    private static $disallowed_elements = [
        ElementMedia::class,
    ];

    /**
     * @var array
     */
    private static $extensions = [
        ElementalAreasExtension::class,
    ];

    /**
     * @inheritdoc
     */
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Metadata',
            'TabSetOne',
            'StudentTypes',
            'Intro',
            'ApplyCTA',
        ]);

        // tabs
        $fields->addFieldsToTab('Root.ProgrammeInfo', [
            DropdownField::create('QualificationTypeID', 'Qualification Type', QualificationType::get()->filter([
                'Title' => 'Short Course'
            ])->map('ID', 'Title')),
        ]);

        // tabs
        $fields->addFieldsToTab('Root.Search & Taxonomy', [
            DropdownField::create('LocationID', 'Location', Location::get()->map('ID', 'Title'))
                ->setEmptyString('- Select Location -')
        ]);

        // cta reserve ticket
        if ($this->IsInDB()) {
            $link = LinkField::create('LinkItem', 'CTA', $this->owner);
        } else {
            $link = LiteralField::create('warn', '<p class="message notice">Please save this page before adding a link.</p>');
        }

        return $fields;
    }
}
