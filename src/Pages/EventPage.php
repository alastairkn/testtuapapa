<?php

namespace Tuapapa\TuapapaPackage\Pages;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Blog\Model\BlogPost;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Blog\Model\BlogPostController;

/**
 * Class EventPage
 * @package Tuapapa\TuapapaPackage\Pages
 * @property \SilverStripe\Blog\Model\BlogPost $owner
 */
class EventPage extends BlogPost
{

    /**
     * @var string
     */
    private static $controller_name = BlogPostController::class;

    /**
     * @var string
     */
    private static $table_name = 'App_EventPage';

    /**
     * @var string
     */
    private static $icon_class = 'font-icon-p-articles';

     /**
     * @var string
     */
    private static $singular_name = 'Event';

    /**
     * @var string
     */
    private static $plural_name = 'Events';

    /**
     * Add a custom validator
     * @access public
     * @return RequiredFields
     */
    public function getCMSValidator()
    {

        $requiredfields = ['FeaturedImage', 'FeaturedImageID'];

        return new RequiredFields($requiredfields);
    }

    /*
     * @config
     * @var string
     */
    private static $description = 'Event page';

    /**
     * @return FieldList
     */
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName(['MenuTitle']);

        if ($this->IsInDB()) {
            $fields->addFieldsToTab(
                'Root.Main',
                [
                    LiteralField::create('Header', '<p class="alert alert-info">Create a Header element to display page details.</br>Add a Details Block to configure start/end times and location - these are used when linked throughout the site.</p>'),
                ], 'Content'
            );
        }

        return $fields;
    }
}
