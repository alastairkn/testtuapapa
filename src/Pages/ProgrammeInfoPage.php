<?php

namespace Tuapapa\TuapapaPackage\Pages;

use SilverStripe\Forms\TabSet;
use gorriecoe\Link\Models\Link;
use SilverStripe\ORM\ArrayList;
use SilverStripe\Forms\TextField;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Forms\FieldGroup;
use SilverStripe\TagField\TagField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextareaField;
use Tuapapa\TuapapaPackage\Models\Time;
use SilverStripe\Forms\CheckboxSetField;
use Tuapapa\TuapapaPackage\Models\Intake;
use Tuapapa\TuapapaPackage\Models\Subject;
use SilverStripe\Forms\GridField\GridField;
use Tuapapa\TuapapaPackage\Models\Category;
use Tuapapa\TuapapaPackage\Models\Delivery;
use Tuapapa\TuapapaPackage\Models\Duration;
use Tuapapa\TuapapaPackage\Models\Location;
use Tuapapa\TuapapaPackage\Models\Semester;
use Tuapapa\TuapapaPackage\Models\CareerTag;
use Tuapapa\TuapapaPackage\Models\IntakeItem;
use Tuapapa\TuapapaPackage\Models\TabContent;
use Tuapapa\TuapapaPackage\Models\StudentType;
use Tuapapa\TuapapaPackage\Models\ProgrammeFee;
use Tuapapa\TuapapaPackage\Elements\ElementMedia;
use Tuapapa\TuapapaPackage\Models\ContentLibrary;
use Tuapapa\TuapapaPackage\Models\OccurrenceCode;
use Tuapapa\TuapapaPackage\Elements\ElementCarousel;
use Tuapapa\TuapapaPackage\Models\QualificationType;
use Tuapapa\TuapapaPackage\Elements\ElementAccordion;
use Tuapapa\TuapapaPackage\Elements\ElementEnquiryForm;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Tuapapa\TuapapaPackage\Elements\ElementExploreBlock;
use Tuapapa\TuapapaPackage\Elements\ElementContentLibrary;
use Tuapapa\TuapapaPackage\Elements\ElementGenericContent;
use DNADesign\Elemental\Extensions\ElementalAreasExtension;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use UncleCheese\DisplayLogic\Forms\Wrapper;
use SilverStripe\ORM\Map;
use SilverStripe\Core\Injector\Injector;
use Wilr\SilverStripe\Algolia\Service\AlgoliaPageCrawler;

/**
 * Class ProgrammeInfoPage
 *
 * @package Tuapapa\TuapapaPackage\Pages
 */
class ProgrammeInfoPage extends \Page
{
    /**
     * @var string
     */
    private static $table_name = 'App_ProgrammeInfoPage';

    /**
     * @var string
     */
    private static $singular_name = 'Programme Info Sheet';

    /**
     * @var string
     */
    private static $plural_name = 'Programme Info Sheets';

    /**
     * @var string
     */
    private static $default_sort = '"Title" ASC';

    /**
     * @var string[string]
     */
    private static $db = [
        'Intro' => 'Text',
        'DomesticDurationNumber' => 'Int',
        'DomesticDurationNumberPartTime' => 'Int',
        'DomesticLevel' => 'Int',
        'DomesticCredits' => 'Int',
        'DomesticFees' => 'Int',
        'InternationalDurationNumber' => 'Int',
        'InternationalDurationNumberPartTime' => 'Int',
        'InternationalLevel' => 'Int',
        'InternationalCredits' => 'Int',
        'InternationalFees' => 'Int',
        'ApplyNowURL' => 'Varchar',
        'ApplyNowButtonText' => 'Varchar',
        'ProgrammeCode' => 'Varchar',
        'KISQualificationCode' => 'Varchar',
        'ProgrammeID' => 'Int',
        'DeliverySpecifications' => 'Varchar',
        'ApplicationHide' => 'Boolean',
        'Specialty' => 'Varchar',
        'ShowInSearch' => 'Boolean(1)',
        'Summary' => 'Text',
        'HideBreadcrumbs' => 'Boolean',
    ];

    /**
     * @var string[string]
     */
    private static $summary_fields = [
        'Title' => 'Title',
        'DomesticQualificationType.Title' => 'Qualification Type',
        'DomesticLevel' => 'Level',
        'DomesticCredits' => 'Credits',
        'DomesticFees' => 'Fees',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'DomesticDuration' => Duration::class,
        'DomesticDurationPartTime' => Duration::class,
        'DomesticQualificationType' => QualificationType::class,
        'InternationalDuration' => Duration::class,
        'InternationalDurationPartTime' => Duration::class,
        'InternationalQualificationType' => QualificationType::class,
        'TimeAvailability' => Time::class,
        'Semester' => Semester::class,
        'Subject' => Subject::class,
        'ApplyCTA' => Link::class,
    ];

    /**
     * @var string[]
     */
    private static $has_many = [
        'ProgrammeFees' => ProgrammeFee::class,
        'TabContentDomestic' => TabContent::class . '.Domestic',
        'TabContentInternational' => TabContent::class . '.International',
        'TabContentWhatYouStudy' => TabContent::class . '.WhatYouStudy',
        'TabContentWorkload' => TabContent::class . '.Workload',
        'TabContentEntry' => TabContent::class . '.Entry',
        'TabContentFees' => TabContent::class . '.Fees',
        'TabContentApplication' => TabContent::class . '.Application',
    ];

    /**
     * @var string[]
     */
    private static $many_many = [
        'StudentTypes' => StudentType::class,
        'DomesticDurationTime' => Time::class,
        'DomesticDelivery' => Delivery::class,
        'DomesticLocation' => Location::class,
        'DomesticIntakes' => Intake::class,
        'InternationalDurationTime' => Time::class,
        'InternationalDelivery' => Delivery::class,
        'InternationalLocation' => Location::class,
        'InternationalIntakes' => Intake::class,
        'OccurrenceCodes' => OccurrenceCode::class,
        'Categories' => Category::class,
        'CareerTags' => CareerTag::class,
        'ContentLibraryItems' => ContentLibrary::class,
        'DomesticIntakeItems' => IntakeItem::class,
        'InternationalIntakeItems' => IntakeItem::class
    ];


    /**
     * @var string[]
     */
    private static $belongs_many_many = [
        "ElementExploreBlocks" => ElementExploreBlock::class,
    ];

    /**
     * Enable thirdparty elements
     *
     * @config
     * @var string[]
     */
    private static $allowed_elements = [
        ElementMedia::class,
        ElementAccordion::class,
        ElementContentLibrary::class,
        ElementGenericContent::class,
        ElementEnquiryForm::class,
        ElementCarousel::class
    ];

    /**
     * @var array
     */
    private static $extensions = [
        ElementalAreasExtension::class,
    ];

    public function exportObjectToAlgolia($data)
    {
        $data = array_merge($data, [
            'Subject' => $this->Subject() ? $this->Subject()->Title : null,
            'Tags' => $this->CareerTags ? $this->CareerTags->map('Title')->toArray() : null,
            'Category' => 'Programmes',
            'Intro' => $this->Intro,
            'Categories' => $this->Categories() != null ? $this->Categories()->map('Title')->toArray() : null,
            'QualificationType' => $this->DomesticQualificationType ? $this->DomesticQualificationType->Title : null,
            'CareerTags' => $this->CareerTags() ? $this->CareerTags()->map('Title')->toArray() : null,
            'CategoryPath' => $this->Subject() ? $this->Subject()->Title : null,
            'Duration' => $this->DomesticDuration ? $this->DomesticDurationNumber . ' ' . $this->DomesticDuration->Title : null,
            'Locations' => $this->DomesticLocation() ? $this->DomesticLocation()->map('Title')->toArray() : null,
            'Delivery' => $this->DomesticDelivery() ? $this->DomesticDelivery()->map('Title')->toArray() : null,
            'Summary' => $this->Summary ? $this->Summary : $this->SummaryContent(),
            'ImageURL' => isset($this->FeaturedImageID) ? $this->FeaturedImage->URL : null,
            'objectForTemplate' => Injector::inst()->create(AlgoliaPageCrawler::class, $this)->getMainContent()
        ]);

        $map = new Map(ArrayList::create());

        foreach ($data as $k => $v) {
            $map->push($k, $v);
        }

        return $map;
    }

    /**
     * @inheritdoc
     */
    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Metadata',
            'ApplyCTAID'
        ]);

        // removing this tab prevents stage version being auto created if Dependent pages exist
        $fields->removeByName("Dependent");

        if ($this->IsInDB()) {
            $link = LinkField::create('ApplyCTA', 'CTA Link', $this->owner);
        } else {
            $link = LiteralField::create('warn', '<p class="message notice">Please save this element before adding a link.</p>');
        }

        // main
        $fields->addFieldsToTab('Root.Main', [
            TextareaField::create('Intro', 'Intro')
                ->setDescription('<em>The above content will display in the fixed header element block</em>'),
            CheckboxField::create(
                'HideBreadcrumbs',
                'Hide Breadcrumbs?'
            )->setDescription('Check to hide breadcrumbs'),
            $link
        ], 'ElementalArea');

        // tabs
        $fields->addFieldsToTab('Root.ProgrammeInfo', [
            CheckboxSetField::create('StudentTypes', 'Display Tabs', StudentType::get()->map('ID', 'Title'), [1, 2]),
        ]);

        // programme info tab set one
        $fields->addFieldToTab('Root.ProgrammeInfo', TabSet::create('TabSetOne'), 'Metadata');

        // domestic fields
        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldOrderableRows('Sort'));

        $domesticIntakesConfig = GridFieldConfig_RecordEditor::create();
        $domesticIntakesConfig->addComponent(new GridFieldOrderableRows('Sort'));
        GridField::create('DomesticIntakeItems', 'Multiple Domestic Intakes', $this->DomesticIntakeItems())
            ->setConfig($domesticIntakesConfig);
        $fields->addFieldsToTab('Root.ProgrammeInfo.TabSetOne.Domestic', [
            DropdownField::create(
                'DomesticQualificationTypeID',
                'Qualification Type',
                QualificationType::get()->map('ID', 'Title')
            )->setEmptyString('None'),
            FieldGroup::create(
                CheckboxSetField::create('DomesticDurationTime', '', Time::get()->map('ID', 'Title'))
            )->setTitle('Duration'),
            Wrapper::create(
                FieldGroup::create(
                    NumericField::create('DomesticDurationNumberPartTime', 'Duration Number'),
                    DropdownField::create(
                        'DomesticDurationPartTimeID',
                        'Duration',
                        Duration::get()->map('ID', 'Title')
                    )->setEmptyString('None')
                )->setTitle('Part Time Duration'),
            )->displayIf('DomesticDurationTime')->hasCheckedOption('1')->end(),
            Wrapper::create(
                FieldGroup::create(
                    NumericField::create('DomesticDurationNumber', 'Duration Number'),
                    DropdownField::create(
                        'DomesticDurationID',
                        'Duration',
                        Duration::get()->map('ID', 'Title')
                    )->setEmptyString('None')
                )->setTitle('Full Time Duration')
            )->displayIf('DomesticDurationTime')->hasCheckedOption('2')->end(),
            FieldGroup::create(
                NumericField::create('DomesticLevel', 'Level'),
                NumericField::create('DomesticCredits', 'Credits'),
                NumericField::create('DomesticFees', 'Fees')
            ),
            TagField::create('DomesticDelivery', 'Delivery', Delivery::get())
                ->setCanCreate(false),
            TagField::create('DomesticLocation', 'Location', Location::get())
                ->setCanCreate(false),
            TagField::create('DomesticIntakes', 'Intakes', Intake::get())
                ->setCanCreate(false),
            GridField::create('TabContentDomestic', 'Tab Content Block', $this->TabContentDomestic())
                ->setConfig($config),
        ]);

        // international fields
        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldOrderableRows('Sort'));
        $fields->addFieldsToTab('Root.ProgrammeInfo.TabSetOne.International', [
            DropdownField::create(
                'InternationalQualificationTypeID',
                'Qualification Type',
                QualificationType::get()->map('ID', 'Title')
            )->setEmptyString('None'),
            FieldGroup::create(
                CheckboxSetField::create('InternationalDurationTime', '', Time::get()->map('ID', 'Title'))
            )->setTitle('Duration'),
            Wrapper::create(
                FieldGroup::create(
                    NumericField::create('InternationalDurationNumberPartTime', 'Duration Number'),
                    DropdownField::create(
                        'InternationalDurationPartTimeID',
                        'Duration',
                        Duration::get()->map('ID', 'Title')
                    )->setEmptyString('None'),
                )->setTitle('International Part Time Duration'),
            )->displayIf('InternationalDurationTime')->hasCheckedOption('1')->end(),
            Wrapper::create(
                FieldGroup::create(
                    NumericField::create('InternationalDurationNumber', 'Duration Number'),
                    DropdownField::create(
                        'InternationalDurationID',
                        'Duration',
                        Duration::get()->map('ID', 'Title')
                    )->setEmptyString('None'),
                )->setTitle('International Full Time Duration'),
            )->displayIf('InternationalDurationTime')->hasCheckedOption('2')->end(),
            FieldGroup::create(
                NumericField::create('InternationalLevel', 'Level'),
                NumericField::create('InternationalCredits', 'Credits'),
                NumericField::create('InternationalFees', 'Fees')
            ),
            TagField::create('InternationalDelivery', 'Delivery', Delivery::get())
                ->setCanCreate(false),
            TagField::create('InternationalLocation', 'Location', Location::get())
                ->setCanCreate(false),
            TagField::create('InternationalIntakes', 'Intakes', Intake::get())
                ->setCanCreate(false),

            GridField::create('TabContentInternational', 'Tab Content Block', $this->TabContentInternational())
                ->setConfig($config),
        ]);

        // other tab content
        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldOrderableRows('Sort'));
        $fields->addFieldsToTab('Root.ProgrammeInfo.TabSetOne.WhatYouStudy', [
            GridField::create('TabContentWhatYouStudy', 'Tab Content Block', $this->TabContentWhatYouStudy())
                ->setConfig($config),
        ]);

        // Workload
        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldOrderableRows('Sort'));
        $fields->addFieldsToTab('Root.ProgrammeInfo.TabSetOne.Workload', [
            GridField::create('TabContentWorkload', 'Tab Content Block', $this->TabContentWorkload())
                ->setConfig($config),
        ]);

        // Entry
        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldOrderableRows('Sort'));
        $fields->addFieldsToTab('Root.ProgrammeInfo.TabSetOne.Entry', [
            GridField::create('TabContentEntry', 'Tab Content Block', $this->TabContentEntry())
                ->setConfig($config),
        ]);

        // Fees
        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldOrderableRows('Sort'));
        $config2 = GridFieldConfig_RecordEditor::create();
        $config2->addComponent(new GridFieldOrderableRows('Sort'));
        $fields->addFieldsToTab('Root.ProgrammeInfo.TabSetOne.Fees', [
            GridField::create('ProgrammeFees', 'Programme Fees', $this->ProgrammeFees())
                ->setConfig($config),
            GridField::create('TabContentFees', 'Tab Content Block', $this->TabContentFees())
                ->setConfig($config2),
            LiteralField::create('ContentLibraryTitle', '<p>&nbsp;</p><h2>Content Library</h2>'),
            TagField::create('ContentLibraryItems', 'Other Fee Information', ContentLibrary::get())
                ->setCanCreate(false)
                ->setDescription('<em>These are predefined and display has half of the content width. The Content Library items can be created and edited <a href="/admin/content-library/">here</a></em>'),
        ]);

        // Application
        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldOrderableRows('Sort'));
        $fields->addFieldsToTab('Root.ProgrammeInfo.TabSetOne.Application', [
            GridField::create('TabContentApplication', 'Tab Content Block', $this->TabContentApplication())
                ->setConfig($config),
        ]);

        // Multiple intakes
        $domesticIntakesConfig = GridFieldConfig_RelationEditor::create();
        $domesticIntakesConfig->addComponent(new GridFieldOrderableRows('Sort'));
        $fields->addFieldsToTab('Root.ProgrammeInfo.TabSetOne.MultipleIntakes', [
            GridField::create('DomesticIntakeItems', 'Multiple Domestic Intakes', $this->DomesticIntakeItems())
                ->setConfig($domesticIntakesConfig)
        ]);

        // Multiple intakes
        $intlIntakesConfig = GridFieldConfig_RelationEditor::create();
        $intlIntakesConfig->addComponent(new GridFieldOrderableRows('Sort'));
        $fields->addFieldsToTab('Root.ProgrammeInfo.TabSetOne.MultipleIntakes', [
            GridField::create('InternationalIntakeItems', 'Multiple International Intakes', $this->InternationalIntakeItems())
                ->setConfig($intlIntakesConfig)
        ]);

        // Search and taxonomy tab
        $fields->addFieldsToTab('Root.Search & Taxonomy', [
            TagField::create('Categories', 'Categories', Category::get())
                ->setCanCreate(false),
            DropdownField::create('SubjectID', 'Subject', Subject::get()->map('ID', 'Title'))
                ->setEmptyString('None'),
            TagField::create('CareerTags', 'Career Tags', CareerTag::get()),
            CheckboxField::create('ShowInSearch', 'Show In Search'),
            TextareaField::create('Summary', 'Summary'),
        ]);

        // System integration Tab
        $fields->addFieldsToTab('Root.System Integration', [
            TextField::create('ProgrammeCode', 'Programme Code'),
            TextField::create('KISQualificationCode', 'KIS Qualification Code'),
            TextField::create('ProgrammeID', 'Programme ID'),
            DropdownField::create('SemesterID', 'Advertise for semester', Semester::get()->map('ID', 'Title'))
                ->setEmptyString('None'),
            DropdownField::create('TimeAvailabilityID', 'Time Availability', Time::get()->map('ID', 'Title'))
                ->setEmptyString('None'),
            TagField::create('OccurrenceCodes', 'Occurrence Codes', OccurrenceCode::get())
                ->setCanCreate(false),
            TextField::create('DeliverySpecifications', 'Delivery Specifications'),
            CheckboxField::create('ApplicationHide', 'Hide on Application Page')
                ->setDescription('<em>Example: STAR programmes</em>'),
            TextField::create('ApplyNowURL', 'Apply Now URL')
                ->setDescription('<em>Add a URL here to override the generated Apply Now URL</em>'),
            TextField::create('ApplyNowButtonText', 'Apply Now Button Text')
                ->setDescription('<em>Add a text here to override the text on the Apply Now button</em>'),
        ]);

        return $fields;
    }

    /**
     * @return ArrayList
     * List of data from the programme info tab that displays in the grey boxes
     */
    public function DomesticBoxes()
    {
        $durationString = '';
        $durations = [];
        foreach ($this->DomesticDurationTime()->column('Title') as $durationTime) {
            $durations[$durationTime] = true;
        }

        if (isset($durations['Full-time']) && $durations['Full-time'] === true) {
            $durationString .= '<div class="lowercase mb-2"><span class="inline-block text-right w-6 md:w-7">' . $this->DomesticDurationNumber . '</span> ' . $this->DomesticDuration()->Title . ' <span class="text-lg md:text-xl block font-body ml-8 md:ml-9">Full-time</span></div>';
        }

        if (isset($durations['Part-time']) && $durations['Part-time'] === true) {
            $durationString .= '<div class="lowercase mb-2"><span class="inline-block text-right w-6 md:w-7">' . $this->DomesticDurationNumberPartTime . '</span> ' . $this->DomesticDurationPartTime()->Title . ' <span class="text-lg md:text-xl block font-body ml-8 md:ml-9">Part-time</span></div>';
        }

        $delivery = $this->DomesticDelivery() ? implode('<br>', $this->DomesticDelivery()->column('Title')) : '';
        $multipleIntakes = $this->DomesticIntakeItems();
        $displayMultiple = false;

        if ($multipleIntakes->column('IntakeDetails')) {
            $multipleIntakes = implode('<br>', $multipleIntakes->column('IntakeDetails'));
            $intakes = $multipleIntakes;
            $displayMultiple = true;
        } else {
            $intakes = $this->DomesticIntakes() ? implode('<br>', $this->DomesticIntakes()->column('Title')) : '';
        }

        $location = $this->DomesticLocation() ? implode('<br>', $this->DomesticLocation()->column('Title')) : '';
        $fees = '$' . number_format($this->DomesticFees) . '*';

        return $this->createBoxArrayList(
            $delivery,
            $location,
            $displayMultiple,
            $intakes,
            $this->DomesticLevel,
            $this->DomesticCredits,
            $fees,
            $durationString
        );
    }

    /**
     * @return ArrayList
     * List of data from the programme info tab that displays in the grey boxes
     */
    public function InternationalBoxes()
    {
        $durationString = '';
        $durations = [];
        foreach ($this->InternationalDurationTime()->column('Title') as $durationTime) {
            $durations[$durationTime] = true;
        }

        if (isset($durations['Full-time']) && $durations['Full-time'] === true) {
            $durationString .= '<div class="lowercase mb-2"><span class="inline-block text-right w-6 md:w-7">' . $this->InternationalDurationNumber . '</span> ' . $this->InternationalDuration()->Title . ' <span class="text-lg md:text-xl block font-body ml-8 md:ml-9">Full-time</span></div>';
        }

        if (isset($durations['Part-time']) && $durations['Part-time'] === true) {
            $durationString .= '<div class="lowercase mb-2"><span class="inline-block text-right w-6 md:w-7">' . $this->InternationalDurationNumberPartTime . '</span> ' . $this->InternationalDurationPartTime()->Title . ' <span class="text-lg md:text-xl block font-body ml-8 md:ml-9">Part-time</span></div>';
        }

        $delivery = $this->InternationalDelivery() ? implode('<br>', $this->InternationalDelivery()->column('Title')) : '';
        $location = $this->InternationalLocation() ? implode('<br>', $this->InternationalLocation()->column('Title')) : '';

        $multipleIntakes = $this->InternationalIntakeItems();
        $displayMultiple = false;

        if ($multipleIntakes->column('IntakeDetails')) {
            $multipleIntakes = implode('<br>', $multipleIntakes->column('IntakeDetails'));
            $intakes = $multipleIntakes;
            $displayMultiple = true;
        } else {
            $intakes = $this->InternationalIntakes() ? implode('<br>', $this->InternationalIntakes()->column('Title')) : '';
        }

        $fees = '$' . number_format($this->InternationalFees) . '*';

        return $this->createBoxArrayList(
            $delivery,
            $location,
            $displayMultiple,
            $intakes,
            $this->InternationalLevel,
            $this->InternationalCredits,
            $fees,
            $durationString
        );
    }

    /**
     * @param $delivery
     * @param $location
     * @param $intakes
     * @param $level
     * @param $credits
     * @param $fees
     * @param $durationString
     * @return ArrayList
     */
    private function createBoxArrayList(
        $delivery,
        $location,
        $displayMultiple,
        $intakes,
        $level,
        $credits,
        $fees,
        $durationString
    ) {
        $arrayList = ArrayList::create();
        $arrayData = [
            [
                'Title' => 'Duration',
                'Value' => $durationString,
            ],
            [
                'Title' => 'Level',
                'Value' => $level,
                'JoinedBox' => 1,
            ],
            [
                'Title' => 'Credits',
                'Value' => $credits,
                'JoinedBox' => 2,
            ],
            [
                'Title' => 'Fees',
                'Value' => $fees,
                'Notes' => '&ast;Approximate full qualification tuition fee',

            ],
            [
                'Title' => 'Delivery',
                'Value' => $delivery,
            ],
            [
                'Title' => 'Location',
                'Value' => $location,
            ],
            [
                'Title' => 'Intakes',
                'Value' => $intakes,
                'DisplayMultiple' => $displayMultiple
            ]
        ];

        if ($arrayData[6]['DisplayMultiple']) {
            unset($arrayData[5]);
        }

        // add multidimensional array to list
        foreach ($arrayData as $data) {
            $arrayList->push($data);
        }

        return $arrayList;
    }

    /**
     * @param $tabName
     * @return bool
     */
    public function DisplayTab($tabName)
    {
        if ($this->StudentTypes()) {
            // check student type exists
            $studentTypes = $this->StudentTypes()->filter([
                'Title' => $tabName
            ])->first();

            // return true id exists
            if ($studentTypes) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return mixed
     * returns the disclaimer from the content library at the bottom of every information sheet page
     */
    public function getDisclaimer()
    {
        return ContentLibrary::get()->filter([
            'IsDisclaimer' => 1
        ])->first();
    }

    /**
     * @return mixed
     */
    public function getDomesticFeeBoxes()
    {
        return $this->ProgrammeFees()->filter([
            'StudentType.Title' => 'Domestic'
        ]);
    }

    /**
     * @return mixed
     */
    public function getInternationalFeeBoxes()
    {
        return $this->ProgrammeFees()->filter([
            'StudentType.Title' => 'International'
        ]);
    }

    /**
     * @return boolean
     */
    public function ShowEnquiryButton()
    {
        $elements = $this->ElementalArea();
        $elements = $elements->Elements()->Filter(['ClassName' => ElementEnquiryForm::class]);

        if ($elements && $elements->Count() >= 1) {
            return true;
        }

        return false;
    }

    /**
     * @return ArrayList
     */
    public function getPages()
    {
        $elementPage = $this;
        $breadcrumbs = $elementPage->getBreadcrumbItems();

        return $breadcrumbs;
    }

    /**
     * @return boolean
     */
    public function getHasParentPage()
    {
        $page = $this;
        $hasParent = $page->getParent() ? true : false;

        return $hasParent;
    }

    public function getIsDomesticPartTime()
    {
        $durations = $this->DomesticDurationTime();
        foreach ($durations as $duration) {
            if ($duration->Title == "Part-time") {
                return true;
            }
        }
        return false;
    }

    public function getIsDomesticFullTime()
    {
        $durations = $this->DomesticDurationTime();
        foreach ($durations as $duration) {
            if ($duration->Title == "Full-time") {
                return true;
            }
        }
        return false;
    }

    public function getIsInternationalPartTime()
    {
        $durations = $this->InternationalDurationTime();
        foreach ($durations as $duration) {
            if ($duration->Title == "Part-time") {
                return true;
            }
        }
        return false;
    }

    public function getIsInternationalFullTime()
    {
        $durations = $this->InternationalDurationTime();
        foreach ($durations as $duration) {
            if ($duration->Title == "Full-time") {
                return true;
            }
        }
        return false;
    }
}
