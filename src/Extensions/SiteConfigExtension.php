<?php

namespace Tuapapa\TuapapaPackage\Extensions;

use gorriecoe\Link\Models\Link;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Core\Extension;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;

/**
 * Tuapapa Site Config Extension
 */
class SiteConfigExtension extends Extension
{
    /**
     * @var array
     */
    private static $db = [
        'EnableCommonWebFooter' => 'Boolean',
        'FooterSummary' => 'HTMLText',
        'FooterLearnText' => 'Varchar',
    ];

    /**
     * @var string[]
     */
    private static $has_one = [
        'CommonFooterLogo' => Image::class,
        'CommonFooterLogo2' => Image::class,
        'CTAButton1' => Link::class,
        'CTALink1' => Link::class,
        'CTALink2' => Link::class,
    ];

    /**
     * @var string[]
     */
    private static $owns = [
        'CommonFooterLogo',
        'CommonFooterLogo2',
    ];

    /**
     * @var array
     */
    private static $default_records = [
        ['FooterSummary' => 'is part of Te Pūkenga - New Zealand Institue of Skills and Technology'],
        ['FooterLearnText' => 'Lear with purpose'],
        ['EnableCommonWebFooter' => True],
    ];

    /**
     * @param FieldList $fields
     */
    public function updateCMSFields(FieldList $fields): void
    {
        // common web footer
        $fields->addFieldsToTab('Root.Footer', [
            LiteralField::create('CommonTitle', '<h2>Common Website Footer</h2>'),
            CheckboxField::create('EnableCommonWebFooter', 'Enable common website footer'),
            TextareaField::create('FooterSummary', 'Summary'),
            TextField::create('FooterLearnText', 'Learn text'),
            UploadField::create('CommonFooterLogo', 'Logo left')
                ->setIsMultiUpload(false),
            UploadField::create('CommonFooterLogo2', 'Logo right')
                ->setIsMultiUpload(false),
        ]);

        if ($this->owner->IsInDB()) {
            $fields->addFieldsToTab('Root.Footer', [
                LinkField::create('CTAButton1', 'CTA Button', $this->owner),
                LinkField::create('CTALink1', 'CTA Link 1', $this->owner),
                LinkField::create('CTALink2', 'CTA Link 2', $this->owner),
            ]);
        } else {
            $fields->addFieldsToTab('Root.Footer', [
                LiteralField::create('warn', '<p class="message notice">Please save this item before adding a link.</p>')
            ]);
        }
    }
}
