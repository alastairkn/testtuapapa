<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Assets\Image;
use gorriecoe\Link\Models\Link;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use App\Traits\EditableDataObject;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextareaField;
use Tuapapa\TuapapaPackage\Elements\ElementCategoryBlock;
use SilverStripe\Forms\RequiredFields;

/**
 * Class RelatedItem
 * @package Tuapapa\TuapapaPackage\Models
 */
class RelatedItem extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_RelatedItem';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar',
        'Description' => 'Text'
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'Image' => Image::class,
        'LinkItem' => Link::class,
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Image'
    ];

    /**
     * @var string[]
     */
    private static $belongs_many_many = [
        'ElementCategoryBlocks' => ElementCategoryBlock::class,
    ];

    /**
     * @var string
     */
    private static $singular_name = 'Related Item';

    /**
     * @var string
     */
    private static $plural_name = 'Related Items';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $summary_fields = [
        'Title' => 'Title',
        'Description' => 'Content',
        'Image.StripThumbnail' => 'Image',
    ];

    /**
     * Add a custom validator
     * @access public
     * @return RequiredFields
     */
    public function getCMSValidator()
    {
        $requiredfields = [
            'Description',
            'Title'
        ];

        return new RequiredFields($requiredfields);
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
            'ElementCategoryBlocks',
            'LinkItemID'
        ]);

        if ($this->IsInDB()) {
            $link = LinkField::create('LinkItem', 'Link', $this->owner);
        } else {
            $link = LiteralField::create('warn', '<p class="message notice">Please save this element before adding link.</p>');
        }

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Item Title'),
            TextareaField::create('Description', 'Item Description')->setRows(4),
            $link
        ]);

        return $fields;
    }
}
