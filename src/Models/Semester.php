<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;

/**
 * Class Semester
 * @package Tuapapa\TuapapaPackage\Models
 */
class Semester extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_Semester';

    /**
     * @var string
     */
    private static $singular_name = 'Semester';

    /**
     * @var string
     */
    private static $plural_name = 'Semesters';

     /**
     * @var array
     */
    private static $default_records = [
        ['Title' => 'Semester One'],
        ['Title' => 'Semester Two'],
        ['Title' => 'Semester Three'],
        ['Title' => 'Semester Four'],
    ];

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar'
    ];

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
        ]);

        return $fields;
    }
}
