<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Assets\Image;
use gorriecoe\Link\Models\Link;
use SilverStripe\ORM\ArrayList;
use SilverStripe\ORM\DataObject;
use SilverStripe\View\ArrayData;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use App\Traits\EditableDataObject;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Forms\CheckboxField;
use Tuapapa\TuapapaPackage\Models\Category;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\FieldGroup;
use Tuapapa\TuapapaPackage\Models\RelatedLink;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\LiteralField;
use Tuapapa\TuapapaPackage\Elements\ElementStaff;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Symbiote\GridFieldExtensions\GridFieldEditableColumns;

/**
 * Class Subject
 * @package Tuapapa\TuapapaPackage\Models
 */
class Staff extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_Staff';

    /**
     * @var string
     */
    private static $singular_name = 'Staff';

    /**
     * @var string
     */
    private static $plural_name = 'Staff';

    /**
     * @var string
     */
    private static $default_sort = '"Name" ASC';

    /**
     * @var array
     */
    private static $db = [
        'Name' => 'Varchar',
        'JobTitle' => 'Varchar',
        'Bio' => 'HTMLText',
        'DisplayModal' => 'Boolean(0)',
    ];

    private static $has_one = [
        'Image' => Image::class,
        'Link1' => Link::class,
        'Link2' => Link::class,
    ];

    private static $many_many = [
        'RelatedLinks' => RelatedLink::class,
        'Categories' => Category::class,
    ];

    private static $many_many_extraFields = [
        'RelatedLinks' => [
            'RelatedLinksSortOrder' => 'Int'
        ],
        'Categories' => [
            'JobTitle' => 'Text'
        ]
    ];

    private static $belongs_many_many = [
        'ElementStaff' => ElementStaff::class,
    ];

    private static $owns = [
        'Image'
    ];

    private static $searchable_fields = [
        'Name',
        'JobTitle'
    ];

    private static $summary_fields = [
        'Image.StripThumbnail' => 'Image',
        'Name' => 'Name',
        'JobTitle' => 'Job Title',
        'Bio' => 'Bio',
        'DisplayModal' => 'Display Modal?'
    ];

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        // Remove all fields as we're going to reinsert these in a custom order
        $fields->removeByName([
            'Name',
            'JobTitle',
            'Bio',
            'Image',
            'DisplayModal',
            'Link1ID',
            'Link2ID',
            'RelatedLinks',
            'Categories'
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Name', 'Name'),
            TextField::create('JobTitle', 'Job Title'),
            UploadField::create('Image', 'Image'),
            CheckboxField::create('DisplayModal', 'Display details in modal')->setDescription('Select to enable displaying staff members bio and links within a popup modal'),

            // Add a warning to the modal field if the user has not selected the display in modal checkbox
            FieldGroup::create(
                LiteralField::create('Info', '<p class="alert message">All content below will only be displayed if the Display in modal checkbox is selected above. The content will then be displayed in a popup modal window.</p>'),
            ),

            // All Fields below here are only displayed in the modal window
            HTMLEditorField::create('Bio', 'Bio'),
            LinkField::create('Link1', 'Modal Link', $this->owner)->setDescription('Links available within the modal'),
            LinkField::create('Link2', 'Modal Link', $this->owner)->setDescription('Links available within the modal'),
        ]);

        if ($this->IsInDB()) {
            $relatedLinksField = GridField::create(
                'RelatedLinks',
                'RelatedLinks',
                $this->getManyManyComponents('RelatedLinks'),
                $relatedLinksConfig = GridFieldConfig_RelationEditor::create(10)
            );

            $relatedLinksConfig->addComponent(GridFieldOrderableRows::create('RelatedLinksSortOrder'));
            $relatedLinksConfig->removeComponentsByType(GridFieldAddNewButton::class);
        } else {
            $relatedLinksField = LiteralField::create('warn', '<p class="message notice">Please save this element before adding link.</p>');
        }

        $fields->addFieldsToTab('Root.RelatedLinks', [
            $relatedLinksField
        ]);

        $fields->addFieldsToTab('Root.Categories', [
            GridField::create(
                'Categories',
                'Categories',
                $this->getManyManyComponents('Categories'),
                $categoriesConfig = GridFieldConfig_RelationEditor::create(10)
            )
        ]);

        $categoriesGridFieldEditableColumns = new GridFieldEditableColumns();
        $categoriesGridFieldEditableColumns->setDisplayFields([
            'JobTitle' => [
                'title' => 'Job Title',
                'field' => TextField::class,
            ],
        ]);

        $categoriesConfig->addComponent($categoriesGridFieldEditableColumns);

        return $fields;
    }


    /**
     * @var string
     */
    public function ImageCommonParams()
    {
        return '&fit=crop&auto=format%2C%20compress';
    }

    /**
     * @var string
     */
    public function ImageDefaultParams()
    {
        return 'w=340&h=240';
    }

    /**
     * @var string
     */
    public function ImagePlaceholderParams()
    {
        return 'w=20&h=20';
    }

    /**
     * @return ArrayList
     */
    public function ImageSources()
    {
        return ArrayList::create([
            ArrayData::create([
                'Params' => 'w=340&h=240'
            ]),
            ArrayData::create([
                'Params' => 'w=340&h=240',
                'MaxWidth' => '1024px'
            ]),
            ArrayData::create([
                'Params' => 'w=340&h=240',
                'MaxWidth' => '768px'
            ]),
            ArrayData::create([
                'Params' => 'w=340&h=240',
                'MaxWidth' => '580px'
            ]),
        ]);
    }
}
