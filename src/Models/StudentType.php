<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;

/**
 * Class StudentType
 * @package Tuapapa\TuapapaPackage\Models
 */
class StudentType extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_StudentType';

    /**
     * @var string
     */
    private static $singular_name = 'Student Type';

    /**
     * @var string
     */
    private static $plural_name = 'Student Types';

    /**
     * @var string
     */
    private static $default_sort = '"Title" ASC';

    /**
     * @var array
     */
    private static $default_records = [ ['Title' => 'Domestic'], ['Title' => 'International'], ];

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar'
    ];
}
