<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;

/**
 * Class Location
 * @package Tuapapa\TuapapaPackage\Models
 */
class Location extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_Location';

    /**
     * @var string
     */
    private static $singular_name = 'Location';

    /**
     * @var string
     */
    private static $plural_name = 'Locations';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $default_records = [
        ['Title' => 'Dunedin'],
        ['Title' => 'Cromwell'],
        ['Title' => 'Wanaka'],
        ['Title' => 'Wellington'],
        ['Title' => 'Multi-location'],
        ['Title' => 'N/a'],
    ];

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar'
    ];

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
        ]);

        return $fields;
    }
}
