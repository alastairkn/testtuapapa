<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Assets\Image;
use gorriecoe\Link\Models\Link;
use SilverStripe\ORM\ArrayList;
use SilverStripe\ORM\DataObject;
use SilverStripe\View\ArrayData;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use App\Traits\EditableDataObject;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\AssetAdmin\Forms\UploadField;

/**
 * Class Partner
 * @package Tuapapa\TuapapaPackage\Models
 */
class Partner extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_Partner';

    /**
     * @var string
     */
    private static $singular_name = 'Partner';

    /**
     * @var string
     */
    private static $plural_name = 'Partners';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Sort' => 'Int',
        'Content' => 'HTMLText',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'Link' => Link::class,
        'DefaultLogo' => Image::class,
        'HoverLogo' => Image::class
    ];

    /**
     * @var array
     */
    private static $owns = [
        'DefaultLogo',
        'HoverLogo'
    ];

    /**
     * @var array
     */
    private static $summary_fields = [
        'DefaultLogo.StripThumbnail' => 'Icon',
        'Title' => 'Title',
        'Content.Summary' => 'Content',
    ];

    /**
     * Add a custom validator
     * @access public
     * @return RequiredFields
     */
    public function getCMSValidator()
    {

        $requiredfields = ['Content'];

        if ($this->isInDB()) {
            $requiredfields = ['Content', 'Link'];
        }

        return new RequiredFields($requiredfields);
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
            'ElementPartnerGridID',
            'LinkID',
            'DefaultLogo',
            'HoverLogo'
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Title')
                ->setDescription('<em>Will only display if no images are present</em>'),
            TextareaField::create('Content', 'Content')->setRows(4),
            UploadField::create('DefaultLogo', 'Default Logo')
                ->setDescription('<em>Default PNG, white fill. Image dimensions 340px x 30px</em>'),
            UploadField::create('HoverLogo', 'Hover Logo')
                ->setDescription('<em>Hover PNG, primary colour fill. Image dimensions 340px x 30px, must match the size of the default image above</em>'),
            LinkField::create('Link', 'Link', $this->owner)
        ]);

        return $fields;
    }

    /**
     * @var string
     */
    public function ImageCommonParams()
    {
        return '&fit=crop&auto=format%2C%20compress';
    }

    /**
     * @var string
     */
    public function ImageDefaultParams()
    {
        return 'w=340&h=140';
    }

    /**
     * @var string
     */
    public function ImagePlaceholderParams()
    {
        return 'w=20&h=20';
    }

    /**
     * @return ArrayList
     */
    public function ImageSources()
    {
        return ArrayList::create([
            ArrayData::create([
                'Params' => 'w=340&h=140'
            ]),
            ArrayData::create([
                'Params' => 'w=340&h=140',
                'MaxWidth' => '1024px'
            ]),
            ArrayData::create([
                'Params' => 'w=300&h=124',
                'MaxWidth' => '768px'
            ]),
            ArrayData::create([
                'Params' => 'w=248&h=102',
                'MaxWidth' => '580px'
            ]),
        ]);
    }
}
