<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;

/**
 * Class Category
 * @package Tuapapa\TuapapaPackage\Models
 */
class Category extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_Category';

    /**
     * @var string
     */
    private static $singular_name = 'Category';

    /**
     * @var string
     */
    private static $plural_name = 'Categories';

    /**
     * @var string
     */
    private static $default_sort = '"Title" ASC';

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar'
    ];
}
