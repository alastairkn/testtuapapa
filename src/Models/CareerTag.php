<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;

/**
 * Class CareerTag
 * @package Tuapapa\TuapapaPackage\Models
 */
class CareerTag extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_CareerTag';

    /**
     * @var string
     */
    private static $singular_name = 'Career Tag';

    /**
     * @var string
     */
    private static $plural_name = 'Career Tags';

    /**
     * @var string
     */
    private static $default_sort = '"Title" ASC';

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar'
    ];
}
