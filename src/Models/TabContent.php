<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;
use Tuapapa\TuapapaPackage\Pages\ProgrammeInfoPage;
use Tuapapa\TuapapaPackage\Models\ContentLibrary;

/**
 * Class TabContent
 * @package Tuapapa\TuapapaPackage\Models
 */
class TabContent extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_TabContent';

    /**
     * @var string
     */
    private static $singular_name = 'Tab Content';

    /**
     * @var string
     */
    private static $plural_name = 'Tab Content';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar',
        'AdditionalContent' => 'HTMLText',
    ];

    /**
     * @var string[]
     */
    private static $summary_fields = [
        'Title' => 'Title',
        'ContentLibrary.Title' => 'Library Title',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'ContentLibrary' => ContentLibrary::class,
        'Domestic' => ProgrammeInfoPage::class,
        'International' => ProgrammeInfoPage::class,
        'WhatYouStudy' => ProgrammeInfoPage::class,
        'Workload' => ProgrammeInfoPage::class,
        'Entry' => ProgrammeInfoPage::class,
        'Fees' => ProgrammeInfoPage::class,
        'Application' => ProgrammeInfoPage::class,
    ];

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
            'ProgrammeInfoPageID',
            'ContentLibraryID',
            'AdditionalContent',
            'DomesticID',
            'InternationalID',
            'WhatYouStudyID',
            'WorkloadID',
            'EntryID',
            'FeesID',
            'ApplicationID',
        ]);

        $fields->addFieldsToTab('Root.Main', [
            DropdownField::create('ContentLibraryID', 'Content Library Item', ContentLibrary::get()->map('ID', 'Title'))
                ->setDescription('<em>Content Library items can be created and edited <a href="/admin/content-library/">here</a></em>')
                ->setEmptyString('None'),
            TextField::create('Title', 'Title')
                ->setDescription('<em>This field is used in the CSM only and will not display on the front end</em>'),
            HTMLEditorField::create('AdditionalContent', 'Additional content'),
        ]);

        return $fields;
    }
}
