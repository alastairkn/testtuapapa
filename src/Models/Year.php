<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;

/**
 * Class Year
 * @package Tuapapa\TuapapaPackage\Models
 */
class Year extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_Year';

    /**
     * @var string
     */
    private static $singular_name = 'Year';

    /**
     * @var string
     */
    private static $plural_name = 'Years';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $default_records = [
        ['Title' => 'First year'],
        ['Title' => 'Full tuition'],
    ];

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar'
    ];

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
        ]);

        return $fields;
    }
}
