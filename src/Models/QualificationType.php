<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;

/**
 * Class QualificationType
 * @package Tuapapa\TuapapaPackage\Models
 */
class QualificationType extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_QualificationType';

    /**
     * @var string
     */
    private static $singular_name = 'Qualification Type';

    /**
     * @var string
     */
    private static $plural_name = 'Qualification Types';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar',
    ];

    /**
     * @var array
     */
    private static $default_records = [
        ['Title' => 'Certificate'],
        ['Title' => 'Diploma'],
        ['Title' => 'Bachelor'],
        ['Title' => 'Postgraduate'],
        ['Title' => 'Graduate'],
        ['Title' => 'Undergraduate'],
        ['Title' => 'Masters'],
        ['Title' => 'Doctorate'],
        ['Title' => 'Short Course'],
        ['Title' => 'Micro-credential'],
    ];

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
        ]);

        return $fields;
    }
}
