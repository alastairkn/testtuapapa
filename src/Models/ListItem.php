<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use App\Traits\EditableDataObject;
use Tuapapa\TuapapaPackage\Elements\ElementNumberedList;
use SilverStripe\Forms\TextareaField;

/**
 * Class ListItem
 * @package Tuapapa\TuapapaPackage\Models
 */
class ListItem extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_ListItem';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar',
        'Content' => 'Varchar'
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'ElementNumberedList' => ElementNumberedList::class
    ];

    /**
     * @var string
     */
    private static $singular_name = 'List Item';

    /**
     * @var string
     */
    private static $plural_name = 'List Items';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $summary_fields = [
        'Title' => 'Item Title',
        'Content' => 'Item Content',
    ];

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
            'ElementNumberedListID',
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Item Title'),
            TextareaField::create('Content', 'Item Content')->setRows(4)
        ]);

        return $fields;
    }
}
