<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use App\Traits\EditableDataObject;

/**
 * Class IntakeItem
 * @package Tuapapa\TuapapaPackage\Models
 */
class IntakeItem extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_IntakeItem';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'IntakeDetails' => 'Varchar'
    ];

    /**
     * @var string
     */
    private static $singular_name = 'Intake Item';

    /**
     * @var string
     */
    private static $plural_name = 'Intake Items';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $summary_fields = [
        'IntakeDetails' => 'Intakes'
    ];

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort'
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('IntakeDetails', 'Intake Details'),
        ]);

        return $fields;
    }
}
