<?php

namespace Tuapapa\TuapapaPackage\Models;

use gorriecoe\Link\Models\Link;
use gorriecoe\LinkField\LinkField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use App\Traits\EditableDataObject;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

/**
 * Class ContentLibrary
 * @package Tuapapa\TuapapaPackage\Models
 */
class ContentLibrary extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_ContentLibrary';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar',
        'Content' => 'HTMLText',
        'IsDisclaimer' => 'Boolean',
    ];

    /**
     * @var string
     */
    private static $singular_name = 'Content Library';

    /**
     * @var string
     */
    private static $plural_name = 'Content Library';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var string[]
     */
    private static $has_one = [
        'LinkItem' => Link::class,
    ];

    /**
     * @var array
     */
    private static $summary_fields = [
        'Title' => 'Title',
        'Content.Summary' => 'Content',
    ];

    /**
     * Add a custom validator
     * @access public
     * @return RequiredFields
     */
    public function getCMSValidator()
    {
        $requiredfields = [
            'Content',
            'Title'
        ];

        return new RequiredFields($requiredfields);
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
            'ElementContentLibraryID',
            'LinkItemID',
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Title'),
            LinkField::create('LinkItem', 'CTA', $this->owner),
            HTMLEditorField::create('Content', 'Content'),
        ]);

        return $fields;
    }
}
