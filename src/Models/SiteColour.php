<?php

namespace Tuapapa\TuapapaPackage\Models;

use App\Traits\EditableDataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBHTMLText;
use SilverStripe\SiteConfig\SiteConfig;
use TractorCow\Colorpicker\Forms\ColorField;

/**
 * Class SiteColour
 * @package Tuapapa\TuapapaPackage\Models
 */
class SiteColour extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_SiteColour';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'ColourClassName' => 'Varchar',
        'BackgroundColour' => 'Color',
        'TextColour' => 'Color',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'SiteConfig' => SiteConfig::class
    ];

    /**
     * @var string
     */
    private static $singular_name = 'Site Colour';

    /**
     * @var string
     */
    private static $plural_name = 'Site Colours';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $summary_fields = [
        'BackgroundColourSquare' => 'Background Colour',
        'TextColourSquare' => 'Text Colour',
        'ColourClassName' => 'Colour Class Name',
    ];

    /**
     * @var string[]
     */
    private static $casting = [
        'BackgroundColourSquare' => 'HTMLText',
        'TextColourSquare' => 'HTMLText',
    ];

    /**
     * @return mixed
     * This is used for the colour background square in the summary field
     */
    public function BackgroundColourSquare()
    {
        if ($this->BackgroundColour) {
            $square = DBHTMLText::create();
            $square->setValue('<svg width="20" height="20">
            <rect width="20" height="20" style="fill:#' . $this->BackgroundColour . ';stroke-width:1;stroke:rgb(0,0,0)" />
            </svg> #' . $this->BackgroundColour);
            return $square;
        }
    }

    /**
     * @return mixed
     * This is used for the colour text square in the summary field
     */
    public function TextColourSquare()
    {
        $square = DBHTMLText::create();
        $square->setValue('<svg width="20" height="20">
          <rect width="20" height="20" style="fill:#' . $this->TextColour . ';stroke-width:1;stroke:rgb(0,0,0)" />
        </svg> #' . $this->TextColour);
        return $square;
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
            'SiteConfigID',
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('ColourClassName', 'Colour Class Name')
                ->setDescription('<em>This is required to be lowercase with no spaces and will auto format on save if not <strong>e.g main-passion</strong></em>.'),
            ColorField::create('BackgroundColour', 'Background Colour'),
            ColorField::create('TextColour', 'Text Colour')
                ->setDescription('<em>Select a colour to be used for the text when the above colour is used as a background colour.</em>')
        ]);

        return $fields;
    }

    /**
     * Add page title to link text if there isn't one
     */
    protected function onBeforeWrite()
    {
        parent::onBeforeWrite();

        // replace spaces with dashes and lowercase
        // remove special characters
        if ($this->ColourClassName) {
            $this->ColourClassName = preg_replace('/[\W]/', ' ', $this->ColourClassName);
            $this->ColourClassName = str_replace(' ', '-', $this->ColourClassName);
            $this->ColourClassName = strtolower($this->ColourClassName);
        }
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->ColourClassName;
    }
}
