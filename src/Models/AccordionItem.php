<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use Tuapapa\TuapapaPackage\Elements\ElementAccordion;
use App\Traits\EditableDataObject;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

/**
 * Class AccordionItem
 * @package Tuapapa\TuapapaPackage\Models
 */
class AccordionItem extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_AccordionItem';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar',
        'Content' => 'HTMLText'
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'ElementAccordion' => ElementAccordion::class,
    ];

    /**
     * @var string
     */
    private static $singular_name = 'Accordion Item';

    /**
     * @var string
     */
    private static $plural_name = 'Accordion Items';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $summary_fields = [
        'Title' => 'Item Title',
        'Content.Summary' => 'Item Content',
    ];

    /**
     * Add a custom validator
     * @access public
     * @return RequiredFields
     */
    public function getCMSValidator()
    {
        $requiredfields = [
            'Content',
            'Title'
        ];

        return new RequiredFields($requiredfields);
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
            'ElementAccordionID',
            'AdditionalID',
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Item Title'),
            HTMLEditorField::create('Content', 'Item Content')->setRows(4)
        ]);

        return $fields;
    }
}
