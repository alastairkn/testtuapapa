<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;

/**
 * Class Subject
 * @package Tuapapa\TuapapaPackage\Models
 */
class Subject extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_Subject';

    /**
     * @var string
     */
    private static $singular_name = 'Subject';

    /**
     * @var string
     */
    private static $plural_name = 'Subjects';

    /**
     * @var string
     */
    private static $default_sort = '"Title" ASC';

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar'
    ];
}
