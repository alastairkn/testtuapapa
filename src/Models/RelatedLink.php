<?php

namespace Tuapapa\TuapapaPackage\Models;

use gorriecoe\Link\Models\Link;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use App\Traits\EditableDataObject;
use gorriecoe\LinkField\LinkField;
use Tuapapa\TuapapaPackage\Elements\ElementRelatedLinks;

/**
 * Class RelatedLink
 * @package Tuapapa\TuapapaPackage\Models
 */
class RelatedLink extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_RelatedLink';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'Link' => Link::class,
        'ElementRelatedLinks' => ElementRelatedLinks::class
    ];

    /**
     * @var string
     */
    private static $icon = 'font-icon-list';

    /**
     * @var string
     */
    private static $singular_name = 'Related link';

    /**
     * @var string
     */
    private static $plural_name = 'Related links';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $summary_fields = [
        'Link.Title' => 'Link Title',
    ];

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
            'ElementRelatedLinksID',
            'LinkID'
        ]);

        $fields->addFieldsToTab('Root.Main', [
            LinkField::create('Link', 'Link', $this->owner),
        ]);

        return $fields;
    }
}
