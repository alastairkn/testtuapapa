<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;

/**
 * Class FeeType
 * @package Tuapapa\TuapapaPackage\Models
 */
class FeeType extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_FeeType';

    /**
     * @var string
     */
    private static $singular_name = 'Fee Type';

    /**
     * @var string
     */
    private static $plural_name = 'Fee Types';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $default_records = [
        ['Title' => 'Standard'],
        ['Title' => 'Covid-19 Scholarship'],
        ['Title' => 'TTAF'],
    ];

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar'
    ];

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
        ]);

        return $fields;
    }
}
