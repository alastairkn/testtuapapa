<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;

/**
 * Class OccuranceCode
 * @package Tuapapa\TuapapaPackage\Models
 */
class OccurrenceCode extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_OccurrenceCode';

    /**
     * @var string
     */
    private static $singular_name = 'Occurrence Code';

    /**
     * @var string
     */
    private static $plural_name = 'Occurrence Codes';

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Name' => 'Varchar',
    ];

    /**
     * @var string
     */
    private static $default_sort = '"Title" ASC';

    /**
     * @var string[]
     */
    private static $summary_fields = [
        'Title' => 'Code',
        'Name' => 'Name',
    ];

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Code'),
            TextField::create('Name', 'Name'),
        ]);

        return $fields;

    }
}
