<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Assets\Image;
use gorriecoe\Link\Models\Link;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use App\Traits\EditableDataObject;
use gorriecoe\LinkField\LinkField;
use Tuapapa\TuapapaPackage\Elements\ElementTitleCards;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\AssetAdmin\Forms\UploadField;

/**
 * Class TitleCard
 * @package Tuapapa\TuapapaPackage\Models
 */
class TitleCard extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_TitleCard';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar'
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'ElementTitleCards' => ElementTitleCards::class,
        'LinkItem' => Link::class,
        'Image' => Image::class
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Image',
    ];

    /**
     * @var string
     */
    private static $singular_name = 'Title Card';

    /**
     * @var string
     */
    private static $plural_name = 'Title Cards';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $summary_fields = [
        'Title' => 'Card Title',
    ];

    /**
     * Add a custom validator
     * @access public
     * @return RequiredFields
     */
    public function getCMSValidator()
    {
        $requiredfields = [
            'Title',
            'Image'
        ];

        return new RequiredFields($requiredfields);
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
            'ElementTitleCardsID',
            'LinkItemID',
        ]);

        if ($this->IsInDB()) {
            $link = LinkField::create('LinkItem', 'CTA', $this->owner);
        } else {
            $link = LiteralField::create('warn', '<p class="message notice">Please save this item before adding a link.</p>');
        }

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Card Title'),
            $link,
            UploadField::create('Image', 'Image'),
        ]);

        return $fields;
    }
}
