<?php

namespace Tuapapa\TuapapaPackage\Models;

use Tuapapa\TuapapaPackage\Elements\ElementAlert;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use App\Traits\EditableDataObject;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

/**
 * Class SiteAlert
 * @package Tuapapa\TuapapaPackage\Models
 */
class SiteAlert extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_SiteAlert';

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Content' => 'HTMLText'
    ];

    /**
     * @var array
     */
    private static $has_many = [
        'ElementAlerts' => ElementAlert::class
    ];

    /**
     * @var string
     */
    private static $singular_name = 'Site alert';

    /**
     * @var string
     */
    private static $plural_name = 'Site alerts';

    /**
     * @var array
     */
    private static $summary_fields = [
        'Title' => 'Alert Title',
        'Content.Summary' => 'Alert Content',
    ];

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
            'ElementAlerts',
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Item Title'),
            HTMLEditorField::create(
                'Content',
                'Content'
            )->setRows(6)
        ]);

        return $fields;
    }
}
