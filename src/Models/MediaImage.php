<?php

namespace Tuapapa\TuapapaPackage\Models;

use App\Traits\EditableDataObject;
use gorriecoe\LinkField\LinkField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use Tuapapa\TuapapaPackage\Models\SiteColour;
use Tuapapa\TuapapaPackage\Elements\ElementMedia;
use gorriecoe\Link\Models\Link;

/**
 * Class MediaImage
 * @package Tuapapa\TuapapaPackage\Models
 */
class MediaImage extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_MediaImage';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar',
        'Caption' => 'HTMLText',
        'Content' => 'HTMLText',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'ElementMedia' => ElementMedia::class,
        'Image' => Image::class,
        'BackgroundColour' => SiteColour::class,
        'LinkItem' => Link::class,
    ];

    /**
     * @var string[]
     */
    private static $owns = [
      'Image'
    ];

    /**
     * @var string[]
     */
    private static $summary_fields = [
        'Image.StripThumbnail' => 'Image',
        'Title' => 'Title',
        'Caption.Summary' => 'Caption',
        'BackgroundColour.ColourClassName' => 'Colour Class',
    ];

    /**
     * @var string
     */
    private static $singular_name = 'Media Image';

    /**
     * @var string
     */
    private static $plural_name = 'Media Images';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'ElementMediaID',
            'Sort',
            'Image',
            'Caption',
            'LinkItemID',
        ]);

        if ($this->IsInDB()) {
            $link = LinkField::create('LinkItem', 'CTA', $this->owner);
        } else {
            $link = LiteralField::create('warn', '<p class="message notice">Please save this item before adding a link.</p>');
        }

        $fields->addFieldsToTab('Root.Main', [
            UploadField::create('Image', 'Image'),
            DropdownField::create('BackgroundColourID', 'Background Colour', SiteColour::get()->map('ID', 'Title'))
                ->setEmptyString('None'),
            TextareaField::create('Caption', 'Caption')
                ->setDescription('<em>This only displays on the Three Stacked block</em>'),
            TextField::create('Title', 'Title'),
            HTMLEditorField::create('Content', 'Content'),
            $link,
        ]);

        return $fields;
    }
}
