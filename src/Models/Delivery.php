<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;

/**
 * Class Delivery
 * @package Tuapapa\TuapapaPackage\Models
 */
class Delivery extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_Delivery';

    /**
     * @var string
     */
    private static $singular_name = 'Delivery';

    /**
     * @var string
     */
    private static $plural_name = 'Deliveries';

    /**
     * @var array
     */
    private static $default_records = [
        ['Title' => 'On Campus'],
        ['Title' => 'Blended'],
        ['Title' => 'Distance'],
        ['Title' => 'Work-based'],
        ['Title' => 'Online'],
    ];

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar'
    ];

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
        ]);

        return $fields;
    }
}
