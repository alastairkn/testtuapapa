<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\NumericField;
use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;
use Tuapapa\TuapapaPackage\Pages\ProgrammeInfoPage;

/**
 * Class ProgrammeFee
 * @package Tuapapa\TuapapaPackage\Models
 */
class ProgrammeFee extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_ProgrammeFee';

    /**
     * @var string
     */
    private static $singular_name = 'Programme Fee';

    /**
     * @var string
     */
    private static $plural_name = 'Programme Fees';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Fees' => 'Decimal',
    ];

    /**
     * @var string[]
     */
    private static $has_one = [
        'StudentType' => StudentType::class,
        'Year' => Year::class,
        'FeeType' => FeeType::class,
        'ProgrammeInfoPage' => ProgrammeInfoPage::class,
    ];

    /**
     * @var string[]
     */
    private static $summary_fields = [
        'StudentType.Title' => 'Student Type',
        'Year.Title' => 'Year',
        'FeeType.Title' => 'Fee Type',
        'Fees' => 'Fees',
    ];

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
            'ProgrammeInfoPageID',
            'Fees',
        ]);

        $fields->addFieldsToTab('Root.Main', [
            DropdownField::create('StudentTypeID', 'Student Type', StudentType::get()->map('ID', 'Title'))
                ->setEmptyString('None'),
            DropdownField::create('YearID', 'Year', Year::get()->map('ID', 'Title'))
                ->setEmptyString('None'),
            DropdownField::create('FeeTypeID', 'Fee Type', FeeType::get()->map('ID', 'Title'))
                ->setEmptyString('None'),
            NumericField::create('Fees', 'Fees'),
        ]);

        return $fields;
    }

    public function getFormattedFees() {
        if ($this->Fees) {
            return number_format($this->Fees);
        }

        return '';
    }
}
