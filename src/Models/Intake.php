<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataObject;
use App\Traits\EditableDataObject;

/**
 * Class Intake
 * @package Tuapapa\TuapapaPackage\Models
 */
class Intake extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_Intake';

    /**
     * @var string
     */
    private static $singular_name = 'Intake';

    /**
     * @var string
     */
    private static $plural_name = 'Intakes';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $default_records = [
        ['Title' => 'January'],
        ['Title' => 'Februaru'],
        ['Title' => 'March'],
        ['Title' => 'April'],
        ['Title' => 'May'],
        ['Title' => 'June'],
        ['Title' => 'July'],
        ['Title' => 'August'],
        ['Title' => 'September'],
        ['Title' => 'October'],
        ['Title' => 'November'],
        ['Title' => 'December'],
    ];

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar'
    ];

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
        ]);

        return $fields;
    }
}
