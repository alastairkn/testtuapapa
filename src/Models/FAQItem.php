<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use Tuapapa\TuapapaPackage\Elements\ElementAccordion;
use App\Traits\EditableDataObject;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

/**
 * Class FAQItem
 * @package Tuapapa\TuapapaPackage\Models
 */
class FAQItem extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_FAQItem';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar',
        'Content' => 'HTMLText'
    ];

    /**
     * @var string[]
     */
    private static $belongs_many_many = [
        "ElementAccordions" => ElementAccordion::class,
    ];

    /**
     * @var string
     */
    private static $singular_name = 'FAQ Item';

    /**
     * @var string
     */
    private static $plural_name = 'FAQ Items';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $summary_fields = [
        'Title' => 'Title',
        'Content.Summary' => 'Content',
    ];

    /**
     * Add a custom validator
     * @access public
     * @return RequiredFields
     */
    public function getCMSValidator()
    {
        $requiredfields = [
            'Content',
            'Title'
        ];

        return new RequiredFields($requiredfields);
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
            'ElementAccordionID',
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Item Title'),
            HTMLEditorField::create('Content', 'Item Content')->setRows(4)
        ]);

        return $fields;
    }
}
