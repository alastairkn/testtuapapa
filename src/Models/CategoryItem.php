<?php

namespace Tuapapa\TuapapaPackage\Models;

use SilverStripe\Assets\Image;
use gorriecoe\Link\Models\Link;
use SilverStripe\ORM\DataObject;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use App\Traits\EditableDataObject;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use Tuapapa\TuapapaPackage\Elements\ElementCategoryBlock;

/**
 * Class CategoryItem
 * @package Tuapapa\TuapapaPackage\Models
 */
class CategoryItem extends DataObject
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_CategoryItem';

    /**
     * @var array
     */
    private static $db = [
        'Sort' => 'Int',
        'Title' => 'Varchar',
        'Description' => 'Text'
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'Image' => Image::class,
        'LinkItem' => Link::class,
        'ElementCategoryBlock' => ElementCategoryBlock::class
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Image'
    ];

    /**
     * @var string
     */
    private static $singular_name = 'Category Item';

    /**
     * @var string
     */
    private static $plural_name = 'Category Items';

    /**
     * @var string
     */
    private static $default_sort = '"Sort" ASC';

    /**
     * @var array
     */
    private static $summary_fields = [
        'Title' => 'Item Title',
        'Description' => 'Item Content',
        'Image.StripThumbnail' => 'Image',
    ];

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Sort',
            'ElementCategoryBlockID',
            'AdditionalID',
            'LinkItemID',
            'Content'
        ]);

        if ($this->IsInDB()) {
            $link = LinkField::create('LinkItem', 'Link', $this->owner);
        } else {
            $link = LiteralField::create('warn', '<p class="message notice">Please save this element before adding link.</p>');
        }

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Item Title'),
            TextareaField::create('Description', 'Item Description')->setRows(4),
            $link,
            UploadField::create('Image', 'Image'),
        ]);

        return $fields;
    }
}
