<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Assets\Image;
use gorriecoe\Link\Models\Link;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;
use SilverStripe\Forms\FieldList;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\AssetAdmin\Forms\UploadField;
use Tuapapa\TuapapaPackage\Elements\CoreElement;

/**
 * Class ElementFeaturedBanner
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementFeaturedBanner extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_FeaturedBanner';

    /**
     * @var string
     */
    private static $icon = 'font-icon-block-carousel';

    /**
     * @var string
     */
    private static $singular_name = 'Featured Banner';

    /**
     * @var string
     */
    private static $plural_name = 'Featured Banners';

    /**
     * @var string
     */
    private static $description = 'Banner with Content, CTA and Image(s)';

    /**
     * @var bool
     * remove inline editing
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Content' => 'Varchar',
        'FullBleed' => 'Boolean(0)',
        'ImagePositionLeft' => 'Boolean(0)'
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'CTALink' => Link::class,
        'Image' => Image::class
    ];

    /**
     * @var array
     */
    private static $owns= [
        'Image'
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Featured Banner';
    }

    /**
     * Add a custom validator
     * @access public
     * @return RequiredFields
     */
    public function getCMSValidator()
    {

        $requiredfields = ['Content', 'CTALinkID', 'Item', 'Title', 'Image'];

        if ($this->isInDB()) {
            $requiredfields = ['Content', 'CTALinkID', 'Item', 'Title', 'Image'];
        }

        return new RequiredFields($requiredfields);
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'CTALinkID',
            'Related',
            'BackgroundImage',
            'VideoID',
            'BackgroundSplit',
            'BackgroundType',
            'TitleStyles',
            'SubTitle',
            'Image'
        ]);

        if ($this->IsInDB()) {
            $link = LinkField::create('CTALink', 'Link', $this->owner);
            $fields->addFieldsToTab('Root.Main', [
                $uploader = UploadField::create('Image', 'Image')->setFolderName('Uploads/Featured Banners')
            ]);
        } else {
            $link = LiteralField::create('warn', '<p class="message notice">Please save this element before adding images(s) and link.</p>');
        }

        $fullBleed = $fields->fieldByName('Root.Main.FullBleed')->setDescription('Show background colour as full bleed.');
        $fields->fieldByName('Root.Main.ImagePositionLeft')->setDescription('Check to show image on the left, content right (defaults to image right, content left).');

        $fields->addFieldsToTab('Root.Main', [
            TextareaField::create('Content', 'Content')->setRows(4),
            $link,
        ]);

        return $fields;
    }

    // All of these can be overwritten on a per element/dataobject basis
    public function ImageCommonParams()
    {
        return '&fit=crop&auto=format%2C%20compress';
    }

    public function ImagePlaceholderParams()
    {
        return 'w=40&h=40';
    }

    public function ImageDefaultParams()
    {
        return 'w=600&h=430';
    }

    public function ImageSources()
    {
        return ArrayList::create([
            ArrayData::create([
                'Params' => 'w=600&h=430'
            ]),
            ArrayData::create([
                'Params' => 'w=600&h=430',
                'MaxWidth' => '1024px'
            ]),
            ArrayData::create([
                'Params' => 'w=360&h=240',
                'MaxWidth' => '768px'
            ]),
            ArrayData::create([
                'Params' => 'w=280h=170',
                'MaxWidth' => '520px'
            ]),
        ]);
    }
}
