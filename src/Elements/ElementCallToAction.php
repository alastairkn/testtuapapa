<?php

namespace Tuapapa\TuapapaPackage\Elements;

use Tuapapa\TuapapaPackage\Models\LinkItem;
use Tuapapa\TuapapaPackage\Models\SiteColour;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use SilverStripe\Assets\Image;
use gorriecoe\Link\Models\Link;
use App\Traits\UploadFieldHelper;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;

/**
 * Class ElementCallToAction
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementCallToAction extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Call_To_Action';

    /**
     * @var string
     */
    private static $icon = 'font-icon-link';

    /**
     * @var string
     */
    private static $singular_name = 'Call to Action Block';

    /**
     * @var string
     */
    private static $plural_name = 'Call to Action Block';

    /**
     * @var string
     */
    private static $description = 'Call to Action block with optional image';

    /**
     * @var bool
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Content' => 'HTMLText',
        'ImagePosition' => 'Boolean(0)',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'LinkItem' => Link::class,
        'Image' => Image::class
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Image'
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Call to Action Block';
    }

    /**
     * Add a custom validator
     * @access public
     * @return RequiredFields
     */
    public function getCMSValidator()
    {

        $requiredFields = ['Content', 'Title'];

        return new RequiredFields($requiredFields);
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Related',
            'TitleStyles',
            'LinkItemID',
            'SubTitle',
            'TitleStyle',
            'TitleFont',
            'TitleSize',
            'Options',
            'Related',
            'BackgroundType',
            'BackgroundImage',
            'TitleStyles',
            'BackgroundSplit',
            'VideoID',
            'Image'
        ]);

        if ($this->IsInDB()) {
            $link = LinkField::create('LinkItem', 'CTA', $this->owner);
        } else {
            $link = LiteralField::create('warn', '<p class="message notice">Please save this item before adding logo and link.</p>');
        }

        $image = UploadField::create('Image', 'Image');
        $image->setDescription('Large image that displays to the left or right of CTA block.');
        $image->setFolderName('Uploads/CTA');
        UploadFieldHelper::setFieldAttributes($image, 900, 600);

        $imagePosition = $fields->fieldByName('Root.Main.ImagePosition')->setDescription('Check to show image on the right (defaults to left).');

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Title')
                ->setDescription('<em>This field is used in the CSM only and will not display on the front end</em>'),
            TextareaField::create('Content', 'Content')->setRows(3),
            DropdownField::create('BackgroundColourID', 'Background Colour', SiteColour::get()->map('ID', 'Title'))
                ->setEmptyString('None'),
            $link,
            LiteralField::create('warn', '<p class="message notice">Remove image to display full width CTA block.</p>'),
            $image,
            $imagePosition
        ]);

        return $fields;
    }

    // All of these can be overwritten on a per element/dataobject basis
    public function ImageCommonParams()
    {
        return '&fit=crop&auto=format%2C%20compress';
    }

    public function ImageDefaultParams()
    {
        return 'w=900&h=600';
    }

    public function ImagePlaceholderParams()
    {
        return 'w=40&h=40';
    }

    public function ImageSources()
    {
        return ArrayList::create([
            ArrayData::create([
                'Params' => 'w=900&h=600'
            ]),
            ArrayData::create([
                'Params' => 'w=900&h=600',
                'MaxWidth' => '1024px'
            ]),
            ArrayData::create([
                'Params' => 'w=225&h=350',
                'MaxWidth' => '768px'
            ]),
            ArrayData::create([
                'Params' => 'w=228&h=171',
                'MaxWidth' => '520px'
            ]),
        ]);
    }
}
