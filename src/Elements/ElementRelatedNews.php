<?php

namespace Tuapapa\TuapapaPackage\Elements;

use Tuapapa\TuapapaPackage\Elements\CoreElement;
use SilverStripe\Forms\FieldList;
use SilverStripe\TagField\TagField;
use SilverStripe\Blog\Model\BlogPost;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;

/**
 * Class ElementRelatedNews
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementRelatedNews extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Related_News';

    /**
     * @var string
     */
    private static $icon = 'font-icon-thumbnails';

    /**
     * @var string
     */
    private static $singular_name = 'Related News or Promotions';

    /**
     * @var string
     */
    private static $plural_name = 'Related News or Promotions';

    /**
     * @var string
     */
    private static $description = 'Related News or Promotions';

    /**
     * @var bool
     */
    private static $inline_editable = true;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'RelatedPosts' => BlogPost::class,
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Related News';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'RelatedPosts',
            'SubTitle',
            'TitleStyles',
            'Background',
            'VideoID',
            'BackgroundImage',
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TagField::create('RelatedPosts', 'Related Items', BlogPost::get())
                ->setShouldLazyLoad(false)
                ->setCanCreate(false)
                ->setDescription('<em>Related Items can be created, edited and ordered <a href="/admin/news-and-events/">here</a></em>'),
        ]);

        return $fields;
    }

    // All of these can be overwritten on a per element/dataobject basis
    public function ImageCommonParams()
    {
        return '&fit=crop&auto=format%2C%20compress';
    }

    public function ImageDefaultParams()
    {
        return 'w=380&h=260';
    }

    public function ImagePlaceholderParams()
    {
        return 'w=40&h=40';
    }

    public function ImageSources()
    {
        return ArrayList::create([
            ArrayData::create([
                'Params' => 'w=380&h=260'
            ]),
            ArrayData::create([
                'Params' => 'w=380&h=260',
                'MaxWidth' => '1024px'
            ]),
            ArrayData::create([
                'Params' => 'w=230&h=260',
                'MaxWidth' => '768px'
            ]),
            ArrayData::create([
                'Params' => 'w=288&h=220',
                'MaxWidth' => '520px'
            ]),
        ]);
    }
}
