<?php

namespace Tuapapa\TuapapaPackage\Elements;

use Tuapapa\TuapapaPackage\Models\LinkItem;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use SilverStripe\Assets\Image;
use gorriecoe\Link\Models\Link;
use App\Traits\UploadFieldHelper;
use SilverStripe\Forms\FieldList;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;

/**
 * Class ElementPanelBanner
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementPanelBanner extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Panel_Banner';

    /**
     * @var string
     */
    private static $icon = 'font-icon-block-content';

    /**
     * @var string
     */
    private static $singular_name = 'Split Panel Banner';

    /**
     * @var string
     */
    private static $plural_name = 'Split Panel Banner';

    /**
     * @var string
     */
    private static $description = 'Split Panel Banner';

    /**
     * @var bool
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Content' => 'Varchar',
        'ImagePosition' => 'Boolean'
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'LinkItem' => Link::class,
        'Image' => Image::class
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Image'
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Split Panel Banner';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Related',
            'TitleStyles',
            'LinkItemID',
            'TitleStyle',
            'TitleFont',
            'TitleSize',
            'BackgroundType',
            'BackgroundSplit',
            'VideoID',
            'BackgroundImage'
        ]);

        if ($this->IsInDB()) {
            $link = LinkField::create('LinkItem', 'Link', $this->owner);
        } else {
            $link = LiteralField::create('warn', '<p class="message notice">Please save this element before adding link.</p>');
        }

        $imagePosition = $fields->dataFieldByName('ImagePosition');
        $imagePosition->setDescription('Check to display Image on the right, Content on the left (defaults to Image left, Content right).');

        $image = $fields->dataFieldByName('Image');
        $image->setTitle('Image');
        $image->setFolderName('Uploads/Banners');
        UploadFieldHelper::setFieldAttributes($image, 500, 320);

        $fields->dataFieldByName('BackgroundColourID')->setDescription("<strong>Leave as 'None' for default/secondary colour and theme.</strong>");

        $fields->addFieldsToTab('Root.Main', [
            TextareaField::create('Content', 'Content')->setRows(2),
            $image,
            $imagePosition,
            $link,
        ]);

        return $fields;
    }

      // All of these can be overwritten on a per element/dataobject basis
    public function ImageCommonParams()
    {
        return '&fit=crop&auto=format%2C%20compress';
    }

    public function ImageDefaultParams()
    {
        return 'w=500&h=310';
    }

    public function ImagePlaceholderParams()
    {
        return 'w=40&h=40';
    }

    public function ImageSources()
    {
        return ArrayList::create([
            ArrayData::create([
                'Params' => 'w=500&h=310'
            ]),
            ArrayData::create([
                'Params' => 'w=360&h=324',
                'MaxWidth' => '768px'
            ]),
            ArrayData::create([
                'Params' => 'w=300&h=250',
                'MaxWidth' => '520px'
            ]),
        ]);
    }
}
