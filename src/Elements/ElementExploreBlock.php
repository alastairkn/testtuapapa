<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Versioned\GridFieldArchiveAction;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use Tuapapa\TuapapaPackage\Pages\ProgrammeInfoPage;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Forms\FieldList;

/**
 * Class ElementExploreBlock
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementExploreBlock extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Explore';

    /**
     * @var string
     */
    private static $icon = 'font-icon-search';

    /**
     * @var string
     */
    private static $singular_name = 'Explore Programme';

    /**
     * @var string
     */
    private static $plural_name = 'Explore Programmes';

    /**
     * @var string
     */
    private static $description = 'Explore Element - custom list of Programmes';

    /**
     * @var bool
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar'
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'Programmes' => ProgrammeInfoPage::class,
    ];

    /**
     * @var \string[][]
     */
    private static $many_many_extraFields = [
        'Programmes' => [
            'SortIndex' => 'Int'
        ]
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Explore Programmes';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Programmes',
            'SubTitle',
            'Options',
        ]);

        $config = GridFieldConfig_RelationEditor::create();
        $config->addComponent(new GridFieldOrderableRows('SortIndex'));
        $config->removeComponentsByType(GridFieldAddNewButton::class);
        $config->removeComponentsByType(GridFieldArchiveAction::class);

        $fields->addFieldsToTab('Root.Main', [
            GridField::create('Programmes', 'Programmes', $this->Programmes())
                ->setConfig($config)
                ->setDescription('<em>Programmes be created and edited <a href="/admin/programme-info/">here</a></em>'),
        ]);

        return $fields;
    }

    /**
     * Used to generate a partial caching key for ElementExploreBlock.ss
     */
    public function getProgrammesCacheKey()
    {
        $items = $this->Programmes();

        $fragments = [
            'Items',
            $this->ID,
            $items->max('LastEdited'),
            $this->Parent->getOwnerPage()->LastEdited,
            implode('-', $items->Column('ID')),
        ];

        return implode('__', $fragments);
    }
}
