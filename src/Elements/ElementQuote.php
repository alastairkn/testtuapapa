<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\RequiredFields;
use Tuapapa\TuapapaPackage\Elements\CoreElement;

/**
 * Class ElementQuote
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementQuote extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Quote';

    /**
     * @var string
     */
    private static $icon = 'font-icon-block-file-list';

    /**
     * @var string
     */
    private static $singular_name = 'Quote Block';

    /**
     * @var string
     */
    private static $plural_name = 'Quote Block';

    /**
     * @var string
     */
    private static $description = 'Quote Block';

    /**
     * @var bool
     */
    private static $inline_editable = true;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Quote' => 'Varchar',
        'QuoteAuthor' => 'Varchar',
        'Role' => 'Varchar',
        'Industry' => 'Varchar'
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Quote Block';
    }

    /**
     * Add a custom validator
     * @access public
     * @return RequiredFields
     */
    public function getCMSValidator()
    {
        return new RequiredFields('Quote', 'QuoteAuthor');
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Partners',
            'Related',
            'Subtitle',
            'VideoID',
            'Background',
            'TitleStyles',
            'SubTitle',
            'Title'
        ]);

        $fields->fieldByName('Root.Main.Role')->setDescription('Role/Occupation Title');
        $fields->fieldByName('Root.Main.Industry')->setDescription('Industry Title');
        $fields->addFieldsToTab(
            'Root.Main',
            [
                TextField::create('Title', 'Title')
                    ->setDescription('<em>This field is used in the CSM only and will not display on the front end</em>'),
                TextareaField::create(
                    'Quote',
                    'Quote content'
                )->setRows(4)
            ]
        );

        return $fields;
    }
}
