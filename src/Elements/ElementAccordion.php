<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Forms\CheckboxField;
use Tuapapa\TuapapaPackage\Models\FAQItem;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use Tuapapa\TuapapaPackage\Models\AccordionItem;
use SilverStripe\Forms\FieldList;
use SilverStripe\TagField\TagField;
use SilverStripe\Forms\GridField\GridField;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

/**
 * Class ElementAccordion
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementAccordion extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Accordion';

    /**
     * @var string
     */
    private static $icon = 'font-icon-list';

    /**
     * @var string
     */
    private static $singular_name = 'Accordion Element';

    /**
     * @var string
     */
    private static $plural_name = 'Accordion Element';

    /**
     * @var string
     */
    private static $description = 'Accordion Element';

    /**
     * @var bool
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'CenterTitle' => 'Boolean'
    ];

    /**
     * @var string[]
     */
    private static $has_many = [
        'AccordionItems' => AccordionItem::class,
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'FAQItems' => FAQItem::class,
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Accordion Component';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'FAQItems',
            'SubTitle',
            'TitleStyles',
            'BackgroundType',
            'BackgroundSplit',
            'VideoID',
            'BackgroundImage',
            'AccordionItems'
        ]);

        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldOrderableRows('Sort'));

        $fields->addFieldsToTab('Root.Main', [
            CheckboxField::create(
                'CenterTitle',
                'Center Align Title'
            )->setDescription('Check to center align Title'),
            TagField::create('FAQItems', 'FAQ Items', FAQItem::get())
                ->setShouldLazyLoad(false)
                ->setCanCreate(false)
                ->setDescription('<em>FAQ Items can be created, edited and ordered <a href="/admin/faq-items/">here</a></em>'),
            GridField::create('AccordionItems', 'Additional Accordion Items', $this->AccordionItems())
                ->setConfig($config),
        ]);

        return $fields;
    }

    public function ClassContainsWhiteText()
    {
        if ($this->BackgroundColour && $this->BackgroundColour()->ColourClassName) {
            $className = $this->BackgroundColour()->ColourClassName;
            $bgColour = $this->BackgroundColour()->BackgroundColour;

            if (strpos($className, 'white-background') !== false) {
                return false;
            }

            if (strpos($className, 'white') !== false || strpos($bgColour, 'ffffff') !== false) {
                return true;
            }
        }
    }

    public function AccordionCacheKey()
    {
        $fragments = [
            'Accordion-items',
            $this->ID,
            $this->Parent->getOwnerPage()->LastEdited,
            implode('-', $this->AccordionItems()->Column('ID')),
            $this->AccordionItems()->max('LastEdited')
        ];

        return implode('-_-', $fragments);
    }

    public function FAQCacheKey()
    {
        $fragments = [
            'Accordion-items',
            $this->ID,
            $this->Parent->getOwnerPage()->LastEdited,
            implode('-', $this->AccordionItems()->Column('ID')),
            $this->AccordionItems()->max('LastEdited')
        ];

        return implode('-_-', $fragments);
    }
}
