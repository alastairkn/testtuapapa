<?php

namespace Tuapapa\TuapapaPackage\Elements;

use Tuapapa\TuapapaPackage\Elements\CoreElement;
use Tuapapa\TuapapaPackage\Models\ContentLibrary;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\TagField\TagField;

/**
 * Class ContentLibrary
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementContentLibrary extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_ContentLibrary';

    /**
     * @var string
     */
    private static $icon = 'font-icon-list';

    /**
     * @var string
     */
    private static $singular_name = 'Content Library Element';

    /**
     * @var string
     */
    private static $plural_name = 'Content Library Element';

    /**
     * @var string
     */
    private static $description = 'Content Library Element';

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'AdditionalContent' => 'HTMLText',
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'ContentLibraries' => ContentLibrary::class,
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Content Library';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Background',
            'Title',
            'SubTitle',
            'TitleStyles',
            'ContentLibraries',
            'AdditionalContent',
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Title')
            ->setDescription('<em>This is the block title and doesn\'t display on the front end</em>'),
            TagField::create('ContentLibraries', 'Content Library', ContentLibrary::get())
                ->setShouldLazyLoad(false)
                ->setCanCreate(false)
                ->setDescription('<em>Content Library items can be created, edited and ordered <a href="/admin/content-library/">here</a></em>'),
            HTMLEditorField::create('AdditionalContent', 'Additional custom content'),
        ]);

        return $fields;
    }

    /**
     * Used to generate a partial caching key for ElementContentLibrary.ss
     */
    public function getContentLibrariesCacheKey()
    {
        $items = $this->ContentLibraries();

        $fragments = [
            'Items',
            $this->ID,
            $items->max('LastEdited'),
            implode('-', $items->Column('ID')),
        ];

        return implode('__', $fragments);
    }
}
