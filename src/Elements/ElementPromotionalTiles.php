<?php

namespace Tuapapa\TuapapaPackage\Elements;

use Tuapapa\TuapapaPackage\Elements\CoreElement;
use SilverStripe\Forms\FieldList;
use SilverStripe\TagField\TagField;
use SilverStripe\Blog\Model\BlogPost;

/**
 * Class ElementPromotionalTiles
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementPromotionalTiles extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Promotional_Tiles';

    /**
     * @var string
     */
    private static $icon = 'font-icon-thumbnails';

    /**
     * @var string
     */
    private static $singular_name = 'Promotional Tile';

    /**
     * @var string
     */
    private static $plural_name = 'Promotional Tiles';

    /**
     * @var string
     */
    private static $description = 'Promotional Tiles';

    /**
     * @var bool
     */
    private static $inline_editable = true;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'PromotionalTiles' => BlogPost::class,
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Promotional Tiles';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'PromotionalTiles',
            'SubTitle',
            'TitleStyles',
            'Background',
            'VideoID',
            'BackgroundImage',
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TagField::create('PromotionalTiles', 'Promotional Tiles', BlogPost::get())
                ->setShouldLazyLoad(false)
                ->setCanCreate(false)
                ->setDescription('<em>Promotional Tiles can be created, edited and ordered <a href="/admin/news-and-events/">here</a></em>'),
        ]);

        return $fields;
    }

    /**
     * Used to generate a partial caching key for ElementPromotionalTiles.ss
     */
    public function getPromotionalItemsCacheKey()
    {
        $items = $this->PromotionalTiles();

        $fragments = [
            'Items',
            $this->ID,
            $items->max('LastEdited'),
            implode('-', $items->Column('ID')),
        ];

        return implode('__', $fragments);
    }
}
