<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Assets\Image;
use SilverStripe\Forms\TabSet;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use App\Traits\EditableDataObject;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\ValidationException;
use DNADesign\Elemental\Models\BaseElement;
use Tuapapa\TuapapaPackage\Models\SiteColour;
use SilverStripe\AssetAdmin\Forms\UploadField;
use Tuapapa\TuapapaPackage\Element\ElementHeader;

/**
 * Shared element for all element blocks
 */
class CoreElement extends BaseElement
{
    use EditableDataObject;

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Core';

    /**
     * @var array
     */
    private static $db = [
        'BackgroundType' => 'Enum("None,Colour,Video,Image", "None")',
        'BackgroundSplit' => 'Boolean',
        'VideoID' => 'Varchar',
        'TitleStyle' => 'Enum("Uppercase,Lowercase,LevelTwo", "Uppercase")',
        'TitleFont' => 'Enum("Primary,Secondary", "Primary")',
        'TitleSize' => 'Enum("Large,Medium,Small", "Large")',
        'SubTitle' => 'Varchar',
        'HideBreadcrumbs' => 'Boolean',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'BackgroundImage' => Image::class,
        'BackgroundColour' => SiteColour::class,
        'TextBackgroundColour' => SiteColour::class,
    ];

    /**
     * @var array
     */
    private static $owns = [
        'BackgroundImage',
    ];

    /**
     * @var array
     */
    private static $defaults = [
        'ShowTitle' => 1,
    ];

    /**
     * @return FieldList
     * @throws ValidationException
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Settings',
        ]);

        $className = $this->ClassName;

        if (!$this->getHasParentPage() || !strpos($className, 'ElementHeader') !== false) {
            $fields->removeByName([
                'HideBreadcrumbs',
            ]);
        }

        $fields->addFieldToTab('Root.Main', TabSet::create('Options'));
        $fields->addFieldToTab('Root.Main', LiteralField::create('warn', '<p class="message notice">All Elements are cached so may appear different on the frontend. To bust the cache, ensure the page is published.</p>'));
        $fields->addFieldsToTab('Root.Main.Options.TitleStyles', [
            DropdownField::create('TextBackgroundColourID', 'Text Background Colour', SiteColour::get()->map('ID', 'Title'))
                ->setEmptyString('None'),
            DropdownField::create('TitleStyle', 'Title style', [
                'Uppercase' => 'Uppercase',
                'Lowercase' => 'Lowercase',
                'LevelTwo' => 'LevelTwo',
            ]),
            DropdownField::create('TitleFont', 'Title font', [
                'Primary' => 'Primary',
                'Secondary' => 'Secondary',
            ]),
            DropdownField::create('TitleSize', 'Title size', [
                'Large' => 'Large',
                'Medium' => 'Medium',
                'Small' => 'Small',
            ]),
        ]);
        $fields->addFieldsToTab('Root.Main.Options.Background', [
            DropdownField::create('BackgroundType', 'Background Type', [
                'None' => 'None',
                'Colour' => 'Colour',
                'Video' => 'Video',
                'Image' => 'Image',
            ]),
            CheckboxField::create('BackgroundSplit', 'Background split')
                ->setDescription('<em>Check this box for the coloured background to display as half of the block</em>'),
            DropdownField::create('BackgroundColourID', 'Background Colour', SiteColour::get()->map('ID', 'Title'))
                ->setEmptyString('None'),
            TextField::create('VideoID', 'VideoID')
                ->setDescription('<em>Add your video code. E.g the code at the end of the URL https://youtu.be/<strong>-JtIVGekoJ0</strong></em>'),
            UploadField::create('BackgroundImage', 'Background Image')
                ->setIsMultiUpload(false)
                ->setFolderName('Backgrounds')
                ->setDescription('Assign background image for block. Should be large resolution any height with a minimum 1600px width.')
        ]);

        return $fields;
    }

    public function getType()
    {
        return $this->i18n_singular_name();
    }

    /**
     * @return ArrayList
     */
    public function getPages()
    {
        $elementPage = $this->Parent->getOwnerPage();
        $breadcrumbs = $elementPage->getBreadcrumbItems();

        return $breadcrumbs;
    }

    /**
     * @return boolean
     */
    public function getHasParentPage()
    {
        $page = $this->Parent->getOwnerPage();

        if ($page) {
            $hasParent = $page->getParent() ? true : false;

            return $hasParent;
        }
    }

    /**
     * @return string
     */
    public function getPageTitle()
    {
        return $this->Parent()->getOwnerPage()->Title;
    }

    /**
     * @return string
     */
    public function HTMLToRGB($htmlCode)
    {
        if($htmlCode[0] == '#')
        $htmlCode = substr($htmlCode, 1);

        if (strlen($htmlCode) == 3)  {
            $htmlCode = $htmlCode[0] . $htmlCode[0] . $htmlCode[1] . $htmlCode[1] . $htmlCode[2] . $htmlCode[2];
        }

        $r = hexdec($htmlCode[0] . $htmlCode[1]);
        $g = hexdec($htmlCode[2] . $htmlCode[3]);
        $b = hexdec($htmlCode[4] . $htmlCode[5]);

        return $b + ($g << 0x8) + ($r << 0x10);
    }

    /**
     * @return object
     */
    public function RGBToHSL($RGB)
    {
        $r = 0xFF & ($RGB >> 0x10);
        $g = 0xFF & ($RGB >> 0x8);
        $b = 0xFF & $RGB;

        $r = ((float)$r) / 255.0;
        $g = ((float)$g) / 255.0;
        $b = ((float)$b) / 255.0;

        $maxC = max($r, $g, $b);
        $minC = min($r, $g, $b);

        $l = ($maxC + $minC) / 2.0;

        if ($maxC == $minC) {
            $s = 0;
            $h = 0;
        } else {
            if ($l < 0.5) {
                $s = ($maxC - $minC) / ($maxC + $minC);
            } else {
                $s = ($maxC - $minC) / (2.0 - $maxC - $minC);
            }

            if ($r == $maxC) {
                $h = ($g - $b) / ($maxC - $minC);
            }

            if ($g == $maxC) {
                $h = 2.0 + ($b - $r) / ($maxC - $minC);
            }

            if ($b == $maxC) {
                $h = 4.0 + ($r - $g) / ($maxC - $minC);
            }

            $h = $h / 6.0;
        }

        $h = (int)round(255.0 * $h);
        $s = (int)round(255.0 * $s);
        $l = (int)round(255.0 * $l);

        return (object) Array('hue' => $h, 'saturation' => $s, 'lightness' => $l);
    }

    /**
     * @return boolean
     */
    public function isLightColour()
    {
        $bgHex = $this->BackgroundColour->BackgroundColour ? '#' . $this->BackgroundColour->BackgroundColour : null;

        $rgb = $bgHex ? $this->HTMLToRGB($bgHex) : null;
        $hsl = $rgb ? $this->RGBToHSL($rgb) : null;

        $isLightColour = $hsl && $hsl->lightness > 130 ? true : false;

        return $isLightColour;
    }
}
