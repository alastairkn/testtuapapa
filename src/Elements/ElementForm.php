<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Control\Controller;
use SilverStripe\UserForms\UserForm;
use SilverStripe\Forms\DropdownField;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use SilverStripe\UserForms\Model\UserDefinedForm;
use SilverStripe\UserForms\Control\UserDefinedFormController;
use Tuapapa\TuapapaPackage\Elements\Controller\ElementFormController;

/**
 * Form element that holds custom user forms
 */
class ElementForm extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Form';

    /**
     * @var string
     */
    private static $icon = 'font-icon-block-form';

    /**
     * @var string
     */
    private static $singular_name = 'Form';

    /**
     * @var string
     */
    private static $plural_name = 'Forms';

    /**
     * @var string
     */
    private static $description = 'Form block';

    /**
     * @var string
     */
    private static $controller_class = ElementFormController::class;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'UserForm' => UserDefinedForm::class
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Form Block';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Background',
            'TitleStyles',
            'SubTitle'
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Title')
                ->setDescription('<em>This field is used in the CSM only and will not display on the front end</em>'),
            DropdownField::create('UserFormID', 'Custom Form', UserDefinedForm::get()->map('ID', 'Title'))
                ->setEmptyString('Select a form')
                ->setDescription('<em>Forms can be created and edited in the site settings <a href="/admin/forms/">here</a>.</em>')
        ]);

        return $fields;
    }

    /**
     * @return UserForm
     */
    public function Form()
    {
        $controller = UserDefinedFormController::create($this->UserForm());
        $current = Controller::curr();
        $controller->setRequest($current->getRequest());

        if ($current && $current->getAction() == 'finished') {
            return $controller->renderWith(UserDefinedFormController::class .'_ReceivedFormSubmission');
        }

        $form = $controller->Form();
        $form->setFormAction(
            Controller::join_links(
                $current->Link(),
                'element',
                $this->owner->ID,
                'Form'
            )
        );

        return $form;
    }

    /**
     * @return mixed
     */
    public function Fields() {
        return $this->UserForm()->Fields();
    }

    /**
     * @return mixed
     */
    public function EmailRecipients() {
        return $this->UserForm()->EmailRecipients();
    }

    /**
     * @return mixed
     */
    public function FilteredEmailRecipients() {
        return $this->UserForm()->FilteredEmailRecipients();
    }

    /**
     * @param null $action
     * @return string|void|null
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \SilverStripe\ORM\ValidationException
     */
    public function Link($action = null)
    {
        $current = Controller::curr();

        if ($action === 'finished') {
            return Controller::join_links(
                $current->Link(),
                'finished'
            );
        }

        return parent::Link($action);
    }
}
