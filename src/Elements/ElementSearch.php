<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Forms\CheckboxField;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;

/**
 * Class ElementSearch
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementSearch extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Search';

    /**
     * @var string
     */
    private static $icon = 'font-icon-search';

    /**
     * @var string
     */
    private static $singular_name = 'Search Block';

    /**
     * @var string
     */
    private static $plural_name = 'Search Block';

    /**
     * @var string
     */
    private static $description = 'Search block';

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Placeholder' => 'Varchar',
        'Theme' => 'Boolean(0)'
    ];

    /**
     * @var bool
     */
    private static $inline_editable = true;

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Search Block';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Related',
            'Background',
            'Options',
            'SubTitle'
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Title')
                ->setDescription('<em>This field is used in the CSM only and will not display on the front end</em>'),
            TextField::create('Placeholder', 'Additional placeholder text')
                ->setDescription('<em>E.g "publications" will display as "Search publications" on the search element.</em>'),
            CheckboxField::create(
                'Theme',
                'Check to display search block with light theme (white background colour)'
            )
        ]);


        return $fields;
    }
}
