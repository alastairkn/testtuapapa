<?php

namespace Tuapapa\TuapapaPackage\Elements;

use Tuapapa\TuapapaPackage\Models\SiteAlert;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\DropdownField;

/**
 * Class ElementAlert
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementAlert extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Alert';

    /**
     * @var string
     */
    private static $icon = 'font-icon-info-circled';

    /**
     * @var string
     */
    private static $singular_name = 'Alert Block';

    /**
     * @var string
     */
    private static $plural_name = 'Alert Block';

    /**
     * @var string
     */
    private static $description = 'Alert block';

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'Alert' => SiteAlert::class
    ];

    /**
     * @var bool
     */
    private static $inline_editable = true;

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Alert Block';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'ListItems',
            'Related',
            'Background',
            'Options',
            'SubTitle',
            'Alerts'
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Title')
                ->setDescription('<em>This field is used in the CSM only and will not display on the front end</em>'),
            DropdownField::create('AlertID', 'Site Alert', SiteAlert::get()->map('ID', 'Title'))
                ->setEmptyString('Select an Option')
                ->setDescription('<em>Alert messages can be created and edited in the site settings <a href="/admin/alerts/">here</a>.</em>')
        ]);


        return $fields;
    }
}
