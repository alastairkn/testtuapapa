<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Assets\Image;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\ReadonlyField;
use SilverStripe\Forms\TreeDropdownField;
use Tuapapa\TuapapaPackage\Pages\EventPage;
use SilverStripe\AssetAdmin\Forms\UploadField;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use Tuapapa\TuapapaPackage\Pages\ShortCoursePage;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

/**
 * Class ElementHeader
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementHeader extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Header';

    /**
     * @var string
     */
    private static $icon = 'font-icon-block-file';

    /**
     * @var string
     */
    private static $singular_name = 'Header';

    /**
     * @var string
     */
    private static $plural_name = 'Headers';

    /**
     * @var string
     */
    private static $description = 'Displays a header for the page';

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Content' => 'HTMLText',
        'MediaType' => 'Enum("None,Image,Video", "None")',
        'MediaPosition' => 'Enum("Top,Bottom", "Top")',
        'MediaVideoID' => 'Varchar',
        'LinkType' => 'Enum(array("Page","URL","None"), "None")',
        'LinkText' => 'Varchar',
        'LinkURL' => 'Varchar',
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'Image' => Image::class,
        'Page' => SiteTree::class,
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Image',
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Header';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->addFieldsToTab('Root.Main.Options.Content', [
            HTMLEditorField::create('Content', 'Introduction'),
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Title')
                ->setDescription('<em>This field is used in the CSM only and will not display on the front end</em>'),
        ]);

        // media tab
        $fields->addFieldsToTab('Root.Main.Options.Media', [
            DropdownField::create('MediaType', 'Media type', [
                'None' => 'None',
                'Image' => 'Image',
                'Video' => 'Video',
            ]),
            DropdownField::create('MediaPosition', 'Media position', [
                'Top' => 'Top',
                'Bottom' => 'Bottom',
            ]),
            TextField::create('MediaVideoID', 'VideoID')
                ->setDescription('<em>Add your video code. E.g the code at the end of the URL https://youtu.be/<strong>-JtIVGekoJ0</strong></em>'),
            UploadField::create('Image', 'Image'),
        ]);

        // link tab
        $fields->addFieldsToTab('Root.Main.Options.Link', [
            DropdownField::create('LinkType', 'Link type', [
                'None' => 'None',
                'Page' => 'Page',
                'URL' => 'URL',
            ]),
            TextField::create('LinkText', 'Button text'),
            TreeDropdownField::create('PageID', 'Link page', SiteTree::class),
            TextField::create('LinkURL', 'Link URL'),
        ]);

        if ($this->IsEventPage()) {
            $fields->removeByName([
                'BackgroundImage',
                'BackgroundSplit',
            ]);

            $fields->replaceField(
                'BackgroundType',
                ReadonlyField::create(
                    'BackgroundType',
                    'Background Type'
                )->setValue('Colour')
            );
        }

        return $fields;
    }

    /**
     * @return null
     */
    public function getCTALink()
    {
        switch ($this->LinkType) {
            case 'Page':
                return $this->Page()->exists() ? $this->Page()->Link() : null;
            case 'URL':
                return $this->LinkURL;
            default:
                return null;
        }
    }

    /**
     * @return boolean
     */
    public function isEventPage()
    {
        $eventPage = EventPage::get();

        if ($this->Parent() && $this->Parent()->getOwnerPage()) {


            $elementPage = $this->Parent()->getOwnerPage()->ClassName;

            if ($eventPage->DataClass() == $elementPage) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function isShortCoursePage()
    {
        $shortCoursePage = ShortCoursePage::get();

        if ($this->Parent() && $this->Parent()->getOwnerPage()) {


            $elementPage = $this->Parent()->getOwnerPage()->ClassName;

            if ($shortCoursePage->DataClass() == $elementPage) {
                return true;
            }
        }

        return false;
    }

    public function onBeforeWrite()
    {
        parent::onBeforeWrite();

        if ($this->IsEventPage()) {
            $this->BackgroundType = 'Colour';
        }
    }

    /**
     * @return boolean
     */
    public function getEventCategories()
    {
        $pageID = $this->Parent()->getOwnerPage()->ID;
        $page = SiteTree::get()->byID($pageID);

        if ($page->Categories()) {
            $categories = $page->Categories();

            return $categories;
        }

        return null;
    }

    /**
     * @return Image
     */
    public function getFeaturedImage()
    {
        $pageID = $this->Parent()->getOwnerPage()->ID;
        $page = SiteTree::get()->byID($pageID);

        if ($page->FeaturedImage) {
            return $page->FeaturedImage;
        }

        return null;

    }
}
