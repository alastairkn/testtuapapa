<?php

namespace Tuapapa\TuapapaPackage\Elements;

use Tuapapa\TuapapaPackage\Models\LinkItem;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use SilverStripe\Assets\Image;
use gorriecoe\Link\Models\Link;
use App\Traits\UploadFieldHelper;
use SilverStripe\Forms\FieldList;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\ORM\ValidationResult;
use UncleCheese\DisplayLogic\Forms\Wrapper;
use SilverStripe\AssetAdmin\Forms\UploadField;

/**
 * Class ElementCarousel
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementCarousel extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Carousel';

    /**
     * @var string
     */
    private static $icon = 'font-icon-block-carousel';

    /**
     * @var string
     */
    private static $singular_name = 'Image Carousel';

    /**
     * @var string
     */
    private static $plural_name = 'Image Carousel';

    /**
     * @var string
     */
    private static $description = 'Image Carousel with optional content';

    /**
     * @var bool
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Content' => 'Varchar',
        'CarouselPosition' => 'Boolean',
        'FullWidth' => 'Boolean'
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'Images' => Image::class,
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'LinkItem' => Link::class,
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Images'
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Image Carousel';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Options',
            'SubTitle',
            'Related',
            'TitleStyles',
            'LinkItemID',
            'Images',
            'VideoID',
            'Background',
            'Content',
            'CarouselPosition'
        ]);

        if ($this->IsInDB()) {
            $link = LinkField::create('LinkItem', 'Link', $this->owner);
        } else {
            $link = LiteralField::create('warn', '<p class="message notice">Please save this element before adding link.</p>');
        }

        $fullWidth = $fields->dataFieldByName('FullWidth');
        $fullWidth->setDescription('Check to display carousel as full width');

        $imagePosition = CheckboxField::create(
            'CarouselPosition',
            'Carousel Position'
        );
        $imagePosition->setDescription('Check to display Carousel on the right, Content on the left (defaults to Carousel left, Content right).');

        $content = Wrapper::create(
            TextareaField::create('Content', 'Content')->setRows(2),
            $link,
            $imagePosition
        )->hideUnless('FullWidth')->isNotChecked()->end();

        $images = UploadField::create(
            'Images',
            'Images'
        );

        UploadFieldHelper::setFieldAttributes($images, 1200, 610);

        $images->setIsMultiUpload(true);
        $images->setAllowedMaxFileNumber(10);
        $images->required(true);
        $images->setFolderName('Uploads/Carousel');

        $fields->addFieldsToTab('Root.Main', [
            $images,
            $content
        ]);

        return $fields;
    }

    /**
     * Used to generate a partial caching key for ElementCarousel.ss
     */
    public function getCarouselItemsCacheKey()
    {
        $items = $this->Images();

        $fragments = [
            'Items',
            $this->ID,
            $items->max('LastEdited'),
            implode('-', $items->Column('ID')),
        ];

        return implode('__', $fragments);
    }
}
