<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\TagField\TagField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\ORM\ValidationResult;
use Tuapapa\TuapapaPackage\Models\Partner;
use Tuapapa\TuapapaPackage\Elements\CoreElement;

/**
 * Class ElementPartnerGrid
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementPartnerGrid extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_PartnerGrid';

    /**
     * @var string
     */
    private static $icon = 'font-icon-thumbnails';

    /**
     * @var string
     */
    private static $singular_name = 'Partner Highlight Grid';

    /**
     * @var string
     */
    private static $plural_name = 'Partner Highlight Grid';

    /**
     * @var string
     */
    private static $description = 'Partner Highlight Grid';

    /**
     * @var bool
     * remove inline editing so the grid field works
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'DisplayAll' => 'Boolean',
        'Intro' => 'Text',
    ];

    /**
     * @var array
     */
    private static $defaults = [
        'DisplayAll' => 1,
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'Partners' => Partner::class,
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Partner Highlight Grid';
    }


    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Background',
            'SubTitle',
            'TitleStyles',
            'Options',
            'Partners',
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Title')
                ->setDescription('<em>This field is used in the CSM only and will not display on the front end</em>'),
            CheckboxField::create('DisplayAll', 'Display all partners')
            ->setDescription('<em>If this is unchecked it will display the partners assigned below</em>'),
            TextareaField::create('Intro', 'Intro')
                ->setRows(4),
            TagField::create('Partners', 'Partners', Partner::get())
                ->setShouldLazyLoad(false)
                ->setCanCreate(false)
                ->setDescription('<em>Partners can be created, edited and ordered <a href="/admin/partners/">here</a></em>'),
        ]);

        return $fields;
    }

    /**
     * @return mixed
     */
    public function getAllPartners() {
        return Partner::get();
    }

    /**
     * @return ValidationResult
     */
    public function validate(): ValidationResult
    {
        $validationResult = parent::validate();

        // if not display all and less than 3 partners are added return error
        if ($this->isInDB() && !$this->DisplayAll && $this->Partners()->count() < 3) {
            $validationResult->addFieldError(
                'Partners',
                'This block requires a minimum of 3 partners',
                'error'
            );
        }

        return $validationResult;
    }

    /**
     * Used to generate a partial caching key for ElementPartnerGrid.ss
     */
    public function getPartnerItemsCacheKey()
    {
        $items = $this->Partners();

        $fragments = [
            'Items',
            $this->ID,
            $items->max('LastEdited'),
            implode('-', $items->Column('ID')),
        ];

        return implode('__', $fragments);
    }
}
