<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\GridField\GridField;
use Tuapapa\TuapapaPackage\Models\RelatedLink;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

/**
 * Class ElementRelatedLinks
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementRelatedLinks extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_RelatedLinks';

    /**
     * @var string
     */
    private static $icon = 'font-icon-link';

    /**
     * @var string
     */
    private static $singular_name = 'Related Links';

    /**
     * @var string
     */
    private static $plural_name = 'Related Links';

    /**
     * @var string
     */
    private static $description = 'Displays a list of related links';

    /**
     * @var bool
     * remove inline editing so the grid field works
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $has_many = [
        'RelatedLinks' => RelatedLink::class,
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Related Links';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'RelatedLinks',
            'Related',
            'Background',
            'TitleStyles',
            'VideoID',
            'SubTitle'
        ]);

        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldOrderableRows('Sort'));
        $fields->addFieldsToTab('Root.Main.Options.RelatedLinks', [
            GridField::create('RelatedLinks', 'Related Links', $this->RelatedLinks())
                ->setConfig($config)
        ]);

        return $fields;
    }

    /**
     * Used to generate a partial caching key for ElementRelatedLinks.ss
     */
    public function getRelatedLinksCacheKey()
    {
        $items = $this->RelatedLinks();

        $fragments = [
            'Items',
            $this->ID,
            $items->max('LastEdited'),
            implode('-', $items->Column('ID')),
        ];

        return implode('__', $fragments);
    }
}
