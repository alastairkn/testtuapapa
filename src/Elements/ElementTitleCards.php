<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Forms\TextareaField;
use Tuapapa\TuapapaPackage\Models\TitleCard;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\GridField\GridField;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

/**
 * Class ElementTitleCards
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementTitleCards extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_TitleCards';

    /**
     * @var string
     */
    private static $icon = 'font-icon-list';

    /**
     * @var string
     */
    private static $singular_name = 'Title Cards Element';

    /**
     * @var string
     */
    private static $plural_name = 'Title Cards Element';

    /**
     * @var string
     */
    private static $description = 'Title Cards Element';

    /**
     * @var bool
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Introduction' => 'Text'
    ];

    /**
     * @var string[]
     */
    private static $has_many = [
        'TitleCards' => TitleCard::class,
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Title Cards Component';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Options',
            'SubTitle',
            'TitleStyles',
            'VideoID',
            'Background',
            'TitleCards'
        ]);

        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldOrderableRows('Sort'));

        $fields->addFieldsToTab('Root.Main', [
            TextareaField::create(
                'Introduction',
                'Introduction'
            )->setRows(3),
            GridField::create('TitleCards', 'Title Cards', $this->TitleCards())
                ->setConfig($config),
        ]);

        return $fields;
    }

    /**
     * Used to generate a partial caching key for ElementTitleCards.ss
     */
    public function getTitleCardsCacheKey()
    {
        $items = $this->TitleCards();

        $fragments = [
            'Items',
            $this->ID,
            $items->max('LastEdited'),
            implode('-', $items->Column('ID')),
        ];

        return implode('__', $fragments);
    }
}
