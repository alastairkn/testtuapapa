<?php

namespace Tuapapa\TuapapaPackage\Elements;

use App\Forms\Form\ContactForm;
use SilverStripe\Forms\FieldList;
use SilverStripe\Control\Controller;
use SilverStripe\Forms\LiteralField;
use Tuapapa\TuapapaPackage\Elements\CoreElement;

/**
 * Class ElementEnquiryForm
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementEnquiryForm extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Enquiry_Form';

    /**
     * @var string
     */
    private static $icon = 'font-icon-box';

    /**
     * @var string
     */
    private static $singular_name = 'Enquiry Form';

    /**
     * @var string
     */
    private static $plural_name = 'Enquiry Form';

    /**
     * @var string
     */
    private static $description = 'Enquiry Form';

    /**
     * @var bool
     */
    private static $inline_editable = true;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar'
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Enquiry Form';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Partners',
            'Related',
            'Subtitle',
            'VideoID',
            'Background',
            'TitleStyles',
            'SubTitle'
        ]);

        $message = LiteralField::create('warn', '<p class="message notice">This element automatically renders the contact form.</p>');

        $fields->addFieldToTab(
            'Root.Main',
            $message
        );

        return $fields;
    }

    /**
     * @return ContactForm
     */
    public function getContactForm()
    {
        $controller = Controller::curr();
        $name = 'ContactForm';
        $form = new ContactForm($controller, $name);
        $form->enableSpamProtection();

        return $form;

    }

    /**
     * @return bool
     */
    public function ShowSuccessMessage(): bool
    {
        $controller = Controller::curr();
        return boolval($controller->getRequest()->getVar('cs'));
    }
}
