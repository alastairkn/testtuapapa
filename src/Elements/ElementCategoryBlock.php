<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;
use SilverStripe\Forms\FieldList;
use SilverStripe\TagField\TagField;
use SilverStripe\Blog\Model\BlogTag;
use SilverStripe\Blog\Model\BlogPost;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\GridField\GridField;
use Tuapapa\TuapapaPackage\Models\RelatedItem;
use Tuapapa\TuapapaPackage\Models\CategoryItem;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

/**
 * Class ElementCategoryBlock
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementCategoryBlock extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Category_Block';

    /**
     * @var string
     */
    private static $icon = 'font-icon-drag-handle';

    /**
     * @var string
     */
    private static $singular_name = 'Category Block';

    /**
     * @var string
     */
    private static $plural_name = 'Category Block';

    /**
     * @var string
     */
    private static $description = 'Category Block';

    /**
     * @var bool
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Intro' => 'Text',
        'DisplayType' => 'Boolean',
        'NumberOfItems' => 'Int'
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'RelatedItems' => RelatedItem::class,
        'Tags' => BlogTag::class
    ];

    /**
     * @var string[]
     */
    private static $has_many = [
        'CategoryItems' => CategoryItem::class
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Category Block';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'RelatedItems',
            'SubTitle',
            'TitleStyles',
            'Background',
            'Options',
            'VideoID',
            'CategoryItems',
        ]);

        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldOrderableRows('Sort'));

        $displayType = $fields->dataFieldByName('DisplayType');
        $displayType->setTitle('Check to display first item as Featured (displayed as 2/4 width).');

        $displayList = $fields->dataFieldByName('NumberOfItems');
        $displayList->setTitle('Defaults to 6 items, enter number to override items displayed.');

        $tag = TagField::create('Tags', 'Tags', BlogTag::get())
            ->setShouldLazyLoad(false)
            ->setCanCreate(false);
        $tag->setTitle('Select a Tags to display the most recent items on the frontend automatically.');
        $tag->setDescription('If Tags are present, Related Items or Category Items will not display on the frontend.');

        $fields->addFieldsToTab('Root.Main', [
            $intro = TextareaField::create(
                'Intro',
                'Introduction'
            )->setRows(2),
            $displayType,
            $displayList,
            $tag,
            TagField::create('RelatedItems', 'Related Items', RelatedItem::get())
                ->setShouldLazyLoad(false)
                ->setCanCreate(false)
                ->setDescription('<em>Related Items can be created, edited and ordered <a href="/admin/related-items/">here</a></em>'),
            GridField::create('CategoryItems', 'Additional Category Items', $this->CategoryItems())
                ->setConfig($config)

        ]);

        return $fields;
    }

    // All of these can be overwritten on a per element/dataobject basis
    public function ImageCommonParams()
    {
        return '&fit=crop&auto=format%2C%20compress';
    }

    public function ImagePlaceholderParams()
    {
        return 'w=40&h=40';
    }

    public function ImageDefaultParams()
    {
        $default = $this->DisplayType ? 'w=790&h=380' : 'w=380&h=380';

        return $default;
    }

    public function ImageSources()
    {
        if ($this->DisplayType) {
            $sources = ArrayList::create([
                ArrayData::create([
                    'Params' => 'w=790&h=380'
                ]),
                ArrayData::create([
                    'Params' => 'w=790&h=380',
                    'MaxWidth' => '1024px'
                ]),
                ArrayData::create([
                    'Params' => 'w=470&h=285',
                    'MaxWidth' => '768px'
                ]),
                ArrayData::create([
                    'Params' => 'h=275',
                    'MaxWidth' => '520px'
                ]),
            ]);
        } else {
            $sources = ArrayList::create([
                ArrayData::create([
                    'Params' => 'w=380&h=380'
                ]),
                ArrayData::create([
                    'Params' => 'w=380&h=380',
                    'MaxWidth' => '1024px'
                ]),
                ArrayData::create([
                    'Params' => 'w=230&h=285',
                    'MaxWidth' => '768px'
                ]),
                ArrayData::create([
                    'Params' => 'h=275',
                    'MaxWidth' => '520px'
                ]),
            ]);
        }

        return $sources;
    }

    /**
     * @return ArrayList
     * arra list to merge related items and category items together
     */
    public function getCategoryItemsList()
    {
        $arrayList = ArrayList::create();

        if ($this->Tags()->Count() != 0) {
            $tags = $this->Tags();
            $tagIDs = ArrayList::create();

            foreach ($tags as $tag) {
                $tagIDs->push($tag->ID);
            };

            $items = BlogPost::get()->filter(['Tags.ID' => $tagIDs->items])->sort('PublishDate');

            foreach ($items as $item) {
                $arrayList->push($item);
            }

            $limit = $this->NumberOfItems ? intval($this->NumberOfItems) : 6;

            return $arrayList->Limit($limit);
        }

        // add related items to array list
        if ($this->RelatedItems()) {
            foreach ($this->RelatedItems() as $item) {
                $arrayList->push($item);
            }
        }

        // add category items to array list
        if ($this->CategoryItems()) {
            foreach ($this->CategoryItems() as $item) {
                $arrayList->push($item);
            }
        }

        return $arrayList;
    }

    /**
     * Used to generate a partial caching key for ElementCategoryBlock.ss
     */
    public function getRelatedItemsCacheKey()
    {
        $items = $this->RelatedItems();

        $fragments = [
            'Items',
            $this->ID,
            $items->max('LastEdited'),
            implode('-', $items->Column('ID')),
        ];

        return implode('__', $fragments);
    }
}
