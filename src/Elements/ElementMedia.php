<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\ORM\ValidationResult;
use SilverStripe\Forms\GridField\GridField;
use UncleCheese\DisplayLogic\Forms\Wrapper;
use Tuapapa\TuapapaPackage\Models\MediaImage;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

/**
 * Class ElementMedia
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementMedia extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Media';

    /**
     * @var string
     */
    private static $icon = 'font-icon-block-file';

    /**
     * @var string
     */
    private static $singular_name = 'Media';

    /**
     * @var string
     */
    private static $plural_name = 'Media';

    /**
     * @var string
     */
    private static $description = 'Displays a block with video or images';

    /**
     * @var bool
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Content' => 'HTMLText',
        'MediaType' => 'Enum("None,Images,Video", "None")',
        'ImageDisplayType' => 'Enum("Content Width, Full Width, Three Stacked,Content Images", "Content Width")',
        'MediaVideoID' => 'Varchar',
        'DisplayTranscriptLink' => 'Boolean',
    ];

    /**
     * @var array
     */
    private static $has_many = [
        'Images' => MediaImage::class,
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Media';
    }


    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'SubTitle',
            'TitleStyles',
            'Background',
            'Images',
            'Title'
        ]);

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Title')
                ->setDescription('<em>This field is used in the CSM only and will not display on the front end</em>'),
            DropdownField::create('MediaType', 'Media type', [
                'None' => 'None',
                'Images' => 'Images',
                'Video' => 'Video',
            ])
        ], 'Content');

        // images tab
        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldOrderableRows('Sort'));
        $images = GridField::create('Images', 'Images', $this->Images())
            ->setConfig($config);
        $fields->addFieldsToTab('Root.Main.Options.Images', [
            DropdownField::create('ImageDisplayType', 'Image display type', [
                'Content Width' => 'Content Width',
                'Full Width' => 'Full Width',
                'Three Stacked' => 'Three Stacked',
                'Content Images' => 'Content Images',
            ])->setDescription('<em>Content width and full width will only display the first image in the list below. Three stacked requires 3 images.</em>')->hideUnless('MediaType')->isEqualTo('Images')->end(),
            Wrapper::create('', $images)->hideUnless('MediaType')->isEqualTo('Images')->end()
        ]);

        // video tab
        $fields->addFieldsToTab('Root.Main.Options.Video', [
            TextField::create('MediaVideoID', 'VideoID')->hideUnless('MediaType')->isEqualTo('Video')->end()
                ->setDescription('<em>Add your video code at the end of the URL e.g https://youtu.be/<strong>-JtIVGekoJ0</strong></em>'),
            CheckboxField::create('DisplayTranscriptLink', 'Display transcript link')->hideUnless('MediaType')->isEqualTo('Video')->end()
                ->setDescription('<em>This will display a link to show / hide the content</em>'),
            HTMLEditorField::create('Content', 'Content')->hideUnless('MediaType')->isEqualTo('Video')->end(),
        ]);

        return $fields;
    }

    /**
     * @return ValidationResult
     */
    public function validate(): ValidationResult
    {
        $validationResult = parent::validate();

        // error if the type is ThreeStacked and 3 images aren't uploaded
        if ($this->isInDB() && $this->ImageDisplayType == 'Three Stacked' && $this->Images()->count() != 3) {

            $validationResult->addFieldError(
                'ImageDisplayType',
                'This block requires and only allows 3 images when selecting the image display type "Three Stacked"',
                'error'
            );
        }

        return $validationResult;
    }

       // All of these can be overwritten on a per element/dataobject basis
    public function ImageCommonParams()
    {
        return '&fit=crop&auto=format%2C%20compress';
    }

    public function ImagePlaceholderParams()
    {
        return 'w=40&h=40';
    }

    public function ImageDefaultParams()
    {
        $imageType = $this->ImageDisplayType;
        $default = null;

         switch ($imageType) {
            case "Content Width":
                $default = 'w=800&h=450';
                break;
            case "Full Width":
                $default = 'w=2000&h=720';
                break;
        }

        return $default;
    }

    public function ImageSources()
    {
        $imageType = $this->ImageDisplayType;
        $sources = null;

        switch ($imageType) {
            case "Content Width":
                $sources = ArrayList::create([
                    ArrayData::create([
                        'Params' => 'w=800&h=450'
                    ]),
                    ArrayData::create([
                        'Params' => 'w=800&h=450',
                        'MaxWidth' => '1024px'
                    ]),
                    ArrayData::create([
                        'Params' => 'w=540&h=350',
                        'MaxWidth' => '768px'
                    ]),
                    ArrayData::create([
                        'Params' => 'h=220',
                        'MaxWidth' => '520px'
                    ]),
                ]);
                break;
            case "Full Width":
                $sources = ArrayList::create([
                    ArrayData::create([
                        'Params' => 'w=2000&h=720'
                    ]),
                    ArrayData::create([
                        'Params' => 'w=1024&h=720',
                        'MaxWidth' => '1024px'
                    ]),
                    ArrayData::create([
                        'Params' => 'w=768&h=500',
                        'MaxWidth' => '768px'
                    ]),
                    ArrayData::create([
                        'Params' => 'h=220',
                        'MaxWidth' => '520px'
                    ]),
                ]);
                break;
        }

        return $sources;
    }

    /**
     * Used to generate a partial caching key for ElementMedia.ss
     */
    public function getImagesCacheKey()
    {
        $items = $this->Images();

        $fragments = [
            'Items',
            $this->ID,
            $items->max('LastEdited'),
            implode('-', $items->Column('ID')),
        ];

        return implode('__', $fragments);
    }
}
