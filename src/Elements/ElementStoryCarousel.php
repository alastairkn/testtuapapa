<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Blog\Model\Blog;
use SilverStripe\Forms\FieldList;
use SilverStripe\TagField\TagField;
use SilverStripe\Blog\Model\BlogPost;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Blog\Model\BlogCategory;
use UncleCheese\DisplayLogic\Forms\Wrapper;
use Tuapapa\TuapapaPackage\Elements\CoreElement;

/**
 * Class ElementStoryCarousel
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementStoryCarousel extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_StoryCarousel';

    /**
     * @var string
     */
    private static $icon = 'font-icon-block-carousel';

    /**
     * @var string
     */
    private static $singular_name = 'Carousel Element';

    /**
     * @var string
     */
    private static $plural_name = 'Carousel Element';

    /**
     * @var string
     */
    private static $description = 'Displays a list of news posts in a carousel';

    /**
     * @var bool
     * remove inline editing so the grid field works
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'CarouselType' => 'Enum("Category, Custom")',
        'AlternativeTheme' => 'Boolean',
    ];

    /**
     * @var array
     */
    private static $many_many = [
        'Stories' => BlogPost::class
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'Category' => BlogCategory::class
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Story Carousel';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Stories',
            'Related',
            'BackgroundImage',
            'VideoID',
            'Background',
            'SubTitle',
            'TitleStyles',
        ]);

        $category = $fields->dataFieldByName('CategoryID');
        $fields->addFieldsToTab('Root.Main.Options.Stories', [
            DropdownField::create('CarouselType', 'Carousel type', [
                'Category' => 'Category',
                'Custom' => 'Custom'
            ]),
            CheckboxField::create(
                'AlternativeTheme',
                'Check to display with alternative theme'
            )->setDescription('Change text and arrow colours'),
            $category->hideUnless('CarouselType')->isEqualTo('Category')->end(),
            Wrapper::create('', TagField::create('Stories', 'Stories', BlogPost::get())
                ->setShouldLazyLoad(false)
                ->setCanCreate(false)
                ->setDescription($this->getBlogHolder()))->hideUnless('CarouselType')->isEqualTo('Custom')->end(),
        ]);

        return $fields;
    }

    /**
     * @return DataList|BlogPost[]
     */
    public function getStoriesForTemplate()
    {
        $type = $this->CarouselType;

        if ($type === 'Category') {
            $categoryID = $this->Category->ID;
            $items = BlogPost::get()->filter(['Categories.ID' => $categoryID]);
            return $items;
        } else {
            return $this->Stories();
        }
    }

    /**
     * @return String
     */
    public function getCarouselClass()
    {
        $i = 3;
        $type = $this->CarouselType;

        if ($type === 'Category') {
            $itemCount = $this->getStoriesForTemplate()->count();

            if ($itemCount > $i) {
                return "story-carousel";
            }

            if ($itemCount === $i) {
                return "story-carousel-mobile";
            }

        } else {
            $itemCount = $this->Stories()->count();

            if ($itemCount > $i) {
                return "story-carousel";
            }

            if ($itemCount === $i) {
                return "story-carousel-mobile";
            }
        }
    }

    /**
     * @return String
     */
    public function getBlogHolder()
    {
        if (Blog::get()->First()) {
            $blogUrl = Blog::get()->First()->CmsEditLink();
            $link = 'You can edit items <a href="' . $blogUrl . '"' . '>here</a>';
            return $link;
        }
    }

    /**
     * Used to generate a partial caching key for ElementStoryCarousel.ss
     */
    public function getCarouselItemsCacheKey()
    {
        $items = $this->Stories();

        $fragments = [
            'Items',
            $this->ID,
            $items->max('LastEdited'),
            implode('-', $items->Column('ID')),
        ];

        return implode('__', $fragments);
    }
}
