<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Forms\FieldList;
use Tuapapa\TuapapaPackage\Models\Staff;
use SilverStripe\Forms\GridField\GridField;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use Tuapapa\TuapapaPackage\Models\Category;
use UncleCheese\DisplayLogic\Forms\Wrapper;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;

/**
 * Class ElementStaff
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementStaff extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Staff';

    /**
     * @var string
     */
    private static $icon = 'font-icon-thumbnails';

    /**
     * @var string
     */
    private static $singular_name = 'Staff Tiles';

    /**
     * @var string
     */
    private static $plural_name = 'Staff Tiles';

    /**
     * @var string
     */
    private static $description = 'Staff Tiles';

    /**
     * @var bool
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Intro' => 'Text',
        'ManualStaffSelection' => 'Boolean'
    ];

    private static $has_one = [
        'Category' => Category::class,
    ];

    private static $many_many = [
        'Staff' => Staff::class,
    ];

    private static $many_many_extraFields = [
        'Staff' => [
            'Sort' => 'Int'
        ]
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Staff Tiles';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        // We have no custom style options for this module
        // so remove these from CMS
        $fields->removeByName([
            'SubTitle',
            'Options',
            'Staff',
            'ManualStaffSelection',
            'Category'
        ]);

        $fields->addFieldToTab(
            'Root.Main',
            DropdownField::create('CategoryID', 'Category', Category::get()->map('ID', 'Title'))->setEmptyString('(Select one)')
                ->setDescription('Select a Category to display upon each staff members tile')
        );
        $fields->addFieldToTab(
            'Root.Main',
            CheckboxField::create('ManualStaffSelection', 'Manually Select Staff?')
                ->setDescription('Select this field to manually select staff to appear in this element. If not selected, all staff will be populated from the category selected above')
        );

        if ($this->isInDB()) {
            $config = GridFieldConfig_RelationEditor::create();
            $config->addComponent(new GridFieldOrderableRows('Sort'));

            $fields->addFieldsToTab('Root.Main', [
                Wrapper::create(
                    GridField::create('Staff', 'Staff', $this->Staff())
                        ->setConfig($config)
                )->displayIf("ManualStaffSelection")->isChecked()->end(),
            ]);
        }

        return $fields;
    }

    /**
     * Used to generate a partial caching key for ElementStaff.ss
     */
    public function getStaffItemsCacheKey()
    {
        $items = $this->Staff();

        $fragments = [
            'Staff',
            $this->ID,
            $items->max('LastEdited'),
            implode('-', $items->Column('ID')),
        ];

        return implode('__', $fragments);
    }

    public function getAllStaff()
    {
        if ($this->ManualStaffSelection) {
            return $this->Staff()->sort('Sort ASC');
        } elseif ($id = $this->Category->ID) {
            $id = $this->Category->ID;
            return Staff::get()->filter([
                'Categories.ID' => $id
            ]);
        }
        return false;
    }
}
