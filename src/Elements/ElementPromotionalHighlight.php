<?php

namespace Tuapapa\TuapapaPackage\Elements;

use Tuapapa\TuapapaPackage\Models\LinkItem;
use Tuapapa\TuapapaPackage\Models\SiteColour;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use SilverStripe\Assets\Image;
use gorriecoe\Link\Models\Link;
use SilverStripe\Forms\FieldList;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\RequiredFields;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;

/**
 * Class ElementPromotionalHighlight
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementPromotionalHighlight extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Promotional_Highlight';

    /**
     * @var string
     */
    private static $icon = 'font-icon-picture';

    /**
     * @var string
     */
    private static $singular_name = 'Highlight Block';

    /**
     * @var string
     */
    private static $plural_name = 'Highlight Block';

    /**
     * @var string
     */
    private static $description = 'Highlight Block - customisable block with image and content.';

    /**
     * @var bool
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Category' => 'Varchar',
        'Content' => 'Text',
        'ImagePosition' => 'Boolean(0)',
        'ImageOffset' => 'Boolean(0)'
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'LinkItem' => Link::class,
        'Image' => Image::class
    ];

    /**
     * @var array
     */
    private static $owns = [
        'Image'
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Highlight Block';
    }

    /**
     * Add a custom validator
     * @access public
     * @return RequiredFields
     */
    public function getCMSValidator()
    {

        $requiredfields = ['Content', 'Title'];

        return new RequiredFields($requiredfields);
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Related',
            'Related',
            'BackgroundType',
            'BackgroundImage',
            'TitleStyles',
            'BackgroundSplit',
            'VideoID',
            'LinkItemID',
            'TitleStyles',
            'TitleFont',
            'TitleSize',
            'Images',
            'Image',
            'Options',
        ]);

        if ($this->IsInDB()) {
            $link = LinkField::create('LinkItem', 'CTA', $this->owner);
        } else {
            $link = LiteralField::create('warn', '<p class="message notice">Please save this item before adding image and link.</p>');
        }

        $category = $fields->dataFieldByName('Category');
        $category->setDescription('Optional category/introduction/tag - sits above Title');

        $subtitle = $fields->dataFieldByName('SubTitle');
        $subtitle->setDescription('Optional subtitle - sits below Title');

        $imagePosition = $fields->dataFieldByName('ImagePosition');
        $imagePosition->setDescription('Check to display image on the <strong>right</strong>, defaults to <strong>left</strong>.');

        $imageOffset = $fields->dataFieldByName('ImageOffset');
        $imageOffset->setDescription('Check to display image offset to <strong>bottom</strong>, defaults to <strong>top</strong>.');

        $image = UploadField::create(
            'Image',
            'Image'
        );

        $image->required(true);
        $image->setFolderName('Uploads/Highlights');
        $image->setCustomValidationMessage('Image is required');
        $image->setDescription('Image dimension is: <strong>620w x 700h.</strong>');

        $fields->addFieldsToTab('Root.Main', [
            DropdownField::create('BackgroundColourID', 'Background Colour', SiteColour::get()->map('ID', 'Title'))
                ->setEmptyString('None'),
            TextareaField::create('Content', 'Content')->setRows(2),
            $image,
            $link,
        ]);

        return $fields;
    }

     // All of these can be overwritten on a per element/dataobject basis
    public function ImageCommonParams()
    {
        return '&fit=crop&auto=format%2C%20compress';
    }

    public function ImagePlaceholderParams()
    {
        return 'w=40&h=40';
    }

    public function ImageSources()
    {
        return ArrayList::create([
            ArrayData::create([
                'Params' => 'w=620&h=700'
            ]),
            ArrayData::create([
                'Params' => 'w=620&h=700',
                'MaxWidth' => '1024px'
            ]),
            ArrayData::create([
                'Params' => 'w=340&h=420',
                'MaxWidth' => '768px'
            ]),
            ArrayData::create([
                'Params' => 'w=268&h=350',
                'MaxWidth' => '520px'
            ]),
        ]);
    }
}
