<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\GridField\GridField;
use Tuapapa\TuapapaPackage\Models\ListItem;
use Tuapapa\TuapapaPackage\Models\SiteColour;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;

/**
 * Class ElementNumberedList
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementNumberedList extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_NumberedList';

    /**
     * @var string
     */
    private static $icon = 'font-icon-list';

    /**
     * @var string
     */
    private static $singular_name = 'Numbered List';

    /**
     * @var string
     */
    private static $plural_name = 'Numbered List';

    /**
     * @var string
     */
    private static $description = 'Numbered List element (e.g. event itinerary)';

    /**
     * @var bool
     * remove inline editing so the grid field works
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
    ];

    /**
     * @var array
     */
    private static $has_many = [
        'ListItems' => ListItem::class,
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Numbered List';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'ListItems',
            'Related',
            'Background',
            'TitleStyles',
            'SubTitle',
            'Options',
        ]);

        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldOrderableRows('Sort'));
        $fields->addFieldsToTab('Root.Main', [
            DropdownField::create('BackgroundColourID', 'Background Colour', SiteColour::get()->map('ID', 'Title'))
                ->setEmptyString('None'),
            GridField::create('ListItems', 'List Items', $this->ListItems())
                ->setConfig($config),
        ]);

        return $fields;
    }

    /**
     * Used to generate a partial caching key for ElementNumberedList.ss
     */
    public function getListItemsCacheKey()
    {
        $items = $this->ListItems();

        $fragments = [
            'Items',
            $this->ID,
            $items->max('LastEdited'),
            implode('-', $items->Column('ID')),
        ];

        return implode('__', $fragments);
    }
}
