<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use Tuapapa\TuapapaPackage\Elements\CoreElement;

/**
 * Class ElementEmbedCode
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementEmbedCode extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_EmbedCode';

    /**
     * @var string
     */
    private static $icon = 'font-icon-block-content';

    /**
     * @var string
     */
    private static $singular_name = 'Embed Code Block';

    /**
     * @var string
     */
    private static $plural_name = 'Embed Code Block';

    /**
     * @var string
     */
    private static $description = 'Embed Code Block';

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'EmbedCode' => 'Text',
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Embed Code';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'RelatedLinks',
            'Related',
            'Background',
            'TitleStyles',
            'BackgroundSplit',
            'VideoID',
            'SubTitle',
            'Title',
            'Content'
        ]);

        $fields->addFieldsToTab('Root.Main', [
            $embedCode = TextareaField::create(
                'EmbedCode',
                'Embed Code'
            )->setDescription('<em>Add your HTML here and additional styles/javascript <a href="/admin/settings">here</a> (Code tab)</em>'),
            $embedCode->setRows(24)
        ]);

        return $fields;
    }
}
