<?php

namespace Tuapapa\TuapapaPackage\Elements;

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use Tuapapa\TuapapaPackage\Elements\CoreElement;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

/**
 * Class ElementGenericContent
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementGenericContent extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_GenericContent';

    /**
     * @var string
     */
    private static $icon = 'font-icon-block-content';

    /**
     * @var string
     */
    private static $singular_name = 'Generic Content';

    /**
     * @var string
     */
    private static $plural_name = 'Generic Content';

    /**
     * @var string
     */
    private static $description = 'Generic Content';

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'Content' => 'HTMLText',
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Generic Content';
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'RelatedLinks',
            'Related',
            'Background',
            'TitleStyles',
            'BackgroundSplit',
            'VideoID',
            'SubTitle',
            'Title'
        ]);

        $fields->addFieldsToTab('Root.Main.Options.Content', [
            TextField::create('Title', 'Title')
                ->setDescription('<em>This field is used in the CSM only and will not display on the front end</em>'),
            HTMLEditorField::create('Content', 'Content'),
        ]);

        return $fields;
    }
}
