<?php

namespace Tuapapa\TuapapaPackage\Elements;

use Tuapapa\TuapapaPackage\Elements\CoreElement;
use gorriecoe\Link\Models\Link;
use SilverStripe\Forms\DateField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\TimeField;
use gorriecoe\LinkField\LinkField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\RequiredFields;

/**
 * Class ElementDetailsBlock
 * @package Tuapapa\TuapapaPackage\Elements
 */
class ElementDetailsBlock extends CoreElement
{

    /**
     * @var string
     */
    private static $table_name = 'App_Element_Details_Block';

    /**
     * @var string
     */
    private static $icon = 'font-icon-edit-write';

    /**
     * @var string
     */
    private static $singular_name = 'Details Block';

    /**
     * @var string
     */
    private static $plural_name = 'Details Block';

    /**
     * @var string
     */
    private static $description = 'Details Block - when & where';

    /**
     * @var bool
     */
    private static $inline_editable = false;

    /**
     * @var array
     */
    private static $db = [
        'Title' => 'Varchar',
        'StartDate' => 'Date',
        'EndDate' => 'Date',
        'StartTime' => 'Time',
        'EndTime' => 'Time',
        'DisplayEndDate' => 'Boolean',
        'DisplayEndTime' => 'Boolean'
    ];

    /**
     * @var array
     */
    private static $has_one = [
        'CTALink' => Link::class,
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Details Block';
    }

    /**
     * Add a custom validator
     * @access public
     * @return RequiredFields
     */
    public function getCMSValidator()
    {

        $requiredfields = ['StartDate', 'StartTime'];

        return new RequiredFields($requiredfields);
    }

    /**
     * @return FieldList
     */
    public function getCMSFields(): FieldList
    {
        $fields = parent::getCMSFields();

        $fields->removeByName([
            'Related',
            'Background',
            'TitleStyles',
            'CTALinkID',
            'SubTitle',
            'TitleStyle',
            'TitleFont',
            'TitleSize',
            'Options',
            'Title',
            'History'
        ]);

        if ($this->IsInDB()) {
            $link = LinkField::create('CTALink', 'CTA Link', $this->owner);
        } else {
            $link = LiteralField::create('warn', '<p class="message notice">Please save this element before and link.</p>');
        }

        $endDate = DateField::create('EndDate', 'End Date');
        $endTime = TimeField::create('EndTime', 'End Time');

        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Title')
                ->setDescription('<em>This field is used in the CSM only and will not display on the front end</em>'),
            CheckboxField::create(
                'DisplayEndDate',
                'Display End Date?'
            )->setDescription('Check to display end date field.'),
            CheckboxField::create(
                'DisplayEndTime',
                'Display End Time?'
            )->setDescription('Check to display end time field.'),
            DateField::create('StartDate', 'Start Date'),
            $endDate->hideUnless('DisplayEndDate')->isChecked()->end(),
            TimeField::create('StartTime', 'Start Time'),
            $endTime->hideUnless('DisplayEndTime')->isChecked()->end(),
            $link,
        ]);

        return $fields;
    }

    /**
     * @return string
     */
    public function getFormattedStartDate()
    {
        return $this->obj('StartDate')->Format('dd LLLL yy');
    }

    /**
     * @return string
     */
    public function getFormattedEndDate()
    {
        return $this->obj('EndDate')->Format('dd LLLL yy');
    }

    /**
     * @return string
     */
    public function getFormattedStartTime()
    {
        return $this->obj('StartTime')->Format('h:mm a');
    }

    /**
     * @return string
     */
    public function getMapLink()
    {
        if ($this->IsInDB()) {
            $address = urlencode($this->Address);
            $suburb = urlencode($this->Suburb);
            $state = urlencode($this->State);
            $postcode = urlencode($this->Postcode);
            $country = urlencode($this->CountryName);
            $params = $address . ',' . $suburb . ',' . $state . ',' . $postcode . ',' . $country;
            $mapURL = 'https://maps.google.com/?q=' . $params;

            return $mapURL;
        }
    }

    /**
     * @return string
     */
    public function getFormattedEndTime()
    {
        return $this->obj('EndTime')->Format('h:mm a');
    }
}
