<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\Duration;
use SilverStripe\Admin\ModelAdmin;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class DurationAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        Duration::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'duration';

    /**
     * @var string
     */
    private static $menu_title = 'Duration';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-calendar';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);
        if ($gridFieldName == 'App-Models-Duration') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
