<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\StudentType;
use SilverStripe\Admin\ModelAdmin;

class StudentTypeAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        StudentType::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'student-type';

    /**
     * @var string
     */
    private static $menu_title = 'Student Types';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-globe';
}
