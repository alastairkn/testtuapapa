<?php

namespace Tuapapa\TuapapaPackage\Admins;

use SilverStripe\Admin\ModelAdmin;
use SilverStripe\UserForms\Model\UserDefinedForm;

class FormAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        UserDefinedForm::class
    ];

    /**
     * @var string
     */
    private static $url_segment = 'forms';

    /**
     * @var string
     */
    private static $menu_title = 'Forms';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fas fa-list';
}
