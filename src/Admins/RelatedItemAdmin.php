<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\RelatedItem;
use SilverStripe\Admin\ModelAdmin;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class RelatedItemAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        RelatedItem::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'related-items';

    /**
     * @var string
     */
    private static $menu_title = 'Related Items';

    /**
     * @var string
     */
    private static $menu_icon_class = 'font-icon-block-content';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);

        if ($gridFieldName == 'App-Models-RelatedItem') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
