<?php

namespace Tuapapa\TuapapaPackage\Admins;

use LittleGiant\CatalogManager\ModelAdmin\CatalogPageAdmin;
use Tuapapa\TuapapaPackage\Pages\ShortCoursePage;

/**
 * Class ShortCoursePageAdmin
 *
 * @package Tuapapa\TuapapaPackage\Admins
 */
class ShortCoursePageAdmin extends CatalogPageAdmin
{
    /**
     * @var string
     */
    private static $menu_icon_class = 'fas fa-list';

    /**
     * @var string
     */
    private static $managed_models = [
        ShortCoursePage::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'short-course';

    /**
     * @var string
     */
    private static $menu_title = 'Short Courses';

}
