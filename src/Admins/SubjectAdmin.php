<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\Subject;
use SilverStripe\Admin\ModelAdmin;

class SubjectAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        Subject::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'subject';

    /**
     * @var string
     */
    private static $menu_title = 'Subjects';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-book-open';
}
