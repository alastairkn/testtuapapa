<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\Staff;
use SilverStripe\Admin\ModelAdmin;

class StaffAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        Staff::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'staff';

    /**
     * @var string
     */
    private static $menu_title = 'Staff';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-chalkboard-teacher';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);
        if ($gridFieldName == 'App-Models-Staff') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
