<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\FeeType;
use SilverStripe\Admin\ModelAdmin;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class FeeTypeAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        FeeType::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'fee-type';

    /**
     * @var string
     */
    private static $menu_title = 'Fee Types';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-dollar-sign';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);
        if ($gridFieldName == 'App-Models-FeeType') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
