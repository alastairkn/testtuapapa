<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\Semester;
use SilverStripe\Admin\ModelAdmin;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class SemesterAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        Semester::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'semester';

    /**
     * @var string
     */
    private static $menu_title = 'Semesters';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-calendar';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);
        if ($gridFieldName == 'App-Models-Semester') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
