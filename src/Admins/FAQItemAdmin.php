<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\FAQItem;
use SilverStripe\Admin\ModelAdmin;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class FAQItemAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        FAQItem::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'faq-items';

    /**
     * @var string
     */
    private static $menu_title = 'FAQ Items';

    /**
     * @var string
     */
    private static $menu_icon_class = 'font-icon-plus-circled';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);

        if ($gridFieldName == 'App-Models-FAQItem') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
