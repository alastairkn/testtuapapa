<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Pages\EventPage;
use SilverStripe\Admin\ModelAdmin;
use SilverStripe\Blog\Model\BlogTag;
use SilverStripe\Blog\Model\BlogPost;
use SilverStripe\Blog\Model\BlogCategory;

class NewsAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        EventPage::class => ['title' => 'Event Pages'],
        BlogPost::class => ['title' => 'News Pages'],
        BlogCategory::class => ['title' => 'Categories'],
        BlogTag::class => ['title' => 'Tags'],
    ];

    /**
     * @var string
     */
    private static $url_segment = 'news-and-events';

    /**
     * @var string
     */
    private static $menu_title = 'News & Events';

    /**
     * @var string
     */
    private static $menu_icon_class = 'font-icon-page-multiple';

    /**
     * @var SilverStripe\ORM\DataList
     */
    public function getList()
    {
        $list =  parent::getList();

        if ($this->modelTab === BlogPost::class) {
            $list = $list->filter(['ClassName:not' => EventPage::class]);
        }

        return $list;
    }
}
