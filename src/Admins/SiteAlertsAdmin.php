<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\SiteAlert;
use SilverStripe\Admin\ModelAdmin;

class SiteAlertsAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        SiteAlert::class => ['title' => 'Site Alerts'],
    ];

    /**
     * @var string
     */
    private static $url_segment = 'alerts';

    /**
     * @var string
     */
    private static $menu_title = 'Alerts';

    /**
     * @var string
     */
    private static $menu_icon_class = 'font-icon-info-circled';

    /**
     * @var boolean
     */
    public $showImportForm = false;
}
