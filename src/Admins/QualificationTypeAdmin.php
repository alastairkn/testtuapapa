<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\QualificationType;
use SilverStripe\Admin\ModelAdmin;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class QualificationTypeAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        QualificationType::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'qualification-type';

    /**
     * @var string
     */
    private static $menu_title = 'Qualification Types';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-graduation-cap';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);
        if ($gridFieldName == 'App-Models-QualificationType') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
