<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\Time;
use SilverStripe\Admin\ModelAdmin;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class TimeAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        Time::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'time';

    /**
     * @var string
     */
    private static $menu_title = 'Times';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-clock';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);
        if ($gridFieldName == 'App-Models-Time') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
