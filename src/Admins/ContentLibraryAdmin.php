<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\ContentLibrary;
use SilverStripe\Admin\ModelAdmin;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class ContentLibraryAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        ContentLibrary::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'content-library';

    /**
     * @var string
     */
    private static $menu_title = 'Content Library';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-list';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);
        if ($gridFieldName == 'App-Models-ContentLibrary') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
