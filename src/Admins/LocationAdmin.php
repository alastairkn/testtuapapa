<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\Location;
use SilverStripe\Admin\ModelAdmin;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class LocationAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        Location::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'location';

    /**
     * @var string
     */
    private static $menu_title = 'Locations';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-map-marker';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);
        if ($gridFieldName == 'App-Models-Location') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
