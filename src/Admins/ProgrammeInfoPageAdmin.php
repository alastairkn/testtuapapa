<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Pages\ShortCoursePage;
use Tuapapa\TuapapaPackage\Pages\ProgrammeInfoPage;
use LittleGiant\CatalogManager\ModelAdmin\CatalogPageAdmin;

/**
 * Class ProgrammeInfoPageAdmin
 *
 * @package Tuapapa\TuapapaPackage\Admins
 */
class ProgrammeInfoPageAdmin extends CatalogPageAdmin
{
    /**
     * @var string
     */
    private static $menu_icon_class = 'fas fa-list';

    /**
     * @var string
     */
    private static $managed_models = [
        ProgrammeInfoPage::class,
        ShortCoursePage::class
    ];

    /**
     * @var string
     */
    private static $url_segment = 'programme-info';

    /**
     * @var string
     */
    private static $menu_title = 'Programmes';

    /**
     * @var SilverStripe\ORM\DataList
     */
    public function getList()
    {
        $list =  parent::getList();

        if ($this->modelTab === ProgrammeInfoPage::class) {
            $list = $list->filter(['ClassName:not' => ShortCoursePage::class]);
        }

        return $list;
    }

}
