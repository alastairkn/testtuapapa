<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\Category;
use SilverStripe\Admin\ModelAdmin;

class CategoryAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        Category::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'category';

    /**
     * @var string
     */
    private static $menu_title = 'Categories';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-list';
}
