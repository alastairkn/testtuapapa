<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\Delivery;
use SilverStripe\Admin\ModelAdmin;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class DeliveryAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        Delivery::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'delivery';

    /**
     * @var string
     */
    private static $menu_title = 'Delivery';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-shipping-fast';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);
        if ($gridFieldName == 'App-Models-Delivery') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
