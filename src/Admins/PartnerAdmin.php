<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\Partner;
use SilverStripe\Admin\ModelAdmin;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class PartnerAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        Partner::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'partners';

    /**
     * @var string
     */
    private static $menu_title = 'Partners';

    /**
     * @var string
     */
    private static $menu_icon_class = 'far fa-handshake';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);
        if ($gridFieldName == 'App-Models-Partner') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
