<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\OccurrenceCode;
use SilverStripe\Admin\ModelAdmin;

class OccurrenceCodeAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        OccurrenceCode::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'occurrence-code';

    /**
     * @var string
     */
    private static $menu_title = 'Occurrence Codes';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-hashtag';
}
