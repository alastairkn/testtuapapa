<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\SiteColour;
use SilverStripe\Admin\ModelAdmin;

class SiteColourAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        SiteColour::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'site-colours';

    /**
     * @var string
     */
    private static $menu_title = 'Site Colours';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fas fa-eye-dropper';

    /**
     * @var boolean
     */
    public $showImportForm = false;
}
