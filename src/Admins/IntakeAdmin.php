<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\Intake;
use SilverStripe\Admin\ModelAdmin;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class IntakeAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        Intake::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'intake';

    /**
     * @var string
     */
    private static $menu_title = 'Intakes';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-users';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);
        if ($gridFieldName == 'App-Models-Intake') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
