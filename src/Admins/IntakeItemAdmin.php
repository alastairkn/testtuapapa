<?php

namespace Tuapapa\TuapapaPackage\Admins;

use SilverStripe\Admin\ModelAdmin;
use Tuapapa\TuapapaPackage\Models\IntakeItem;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class IntakeItemAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        IntakeItem::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'intake-item';

    /**
     * @var string
     */
    private static $menu_title = 'Intake Items';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-users';

    /**
     * @param null $id
     * @param null $fields
     * @return mixed
     */
    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        $gridFieldName = $this->sanitiseClassName($this->modelClass);
        if ($gridFieldName == 'App-Models-IntakeItem') {
            $gridField = $form->Fields()->fieldByName($gridFieldName);
            $gridField->getConfig()->addComponent(new GridFieldOrderableRows());
        }

        return $form;
    }
}
