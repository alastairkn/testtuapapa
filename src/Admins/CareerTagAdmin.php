<?php

namespace Tuapapa\TuapapaPackage\Admins;

use Tuapapa\TuapapaPackage\Models\CareerTag;
use SilverStripe\Admin\ModelAdmin;

class CareerTagAdmin extends ModelAdmin
{
    /**
     * @var array
     */
    private static $managed_models = [
        CareerTag::class,
    ];

    /**
     * @var string
     */
    private static $url_segment = 'career-tag';

    /**
     * @var string
     */
    private static $menu_title = 'Career Tags';

    /**
     * @var string
     */
    private static $menu_icon_class = 'fa fa-tags';

}
